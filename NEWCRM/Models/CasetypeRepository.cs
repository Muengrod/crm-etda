﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using NEWCRM.Common;
using System.Xml;
using System.Xml.Linq;
namespace NEWCRM.Models
{
    public class CasetypeRepository : RepositoryBase
    {
        public int AddByEntity(CaseType data)
        {
            var db = this.GetDBContext();
            db.CaseTypes.Add(data);
            return db.SaveChanges();
        }

        public List<CaseType> getAll()
        {
            var db = this.GetDBContext();
            return db.CaseTypes.Where(x => x.catActive == true).ToList();        
        }

        public List<CaseType> getCaseTypeByID()
        {
            var db = this.GetDBContext();
            //if (ConfigurationManager.AppSettings["CASETYPE_REPORTSUMMARYBYCALL"] == "")
                return db.CaseTypes.Where(x => x.catActive == true).ToList();
            /*else
            {
                string[] scatId = ConfigurationManager.AppSettings["CASETYPE_REPORTSUMMARYBYCALL"].Split(',');
                int[] catIDs = new int[scatId.Length];
                for(int i= 0;i< catIDs.Length;i++)
                {
                    catIDs[i] = int.Parse(scatId[i]);
                }
                return db.CaseTypes.Where(x => catIDs.Contains(x.catID)) ).ToList();

            }*/
        }

        /*********************************************************************************************************************************************/
        //append by suksa to manage mail to department by case type/sub type 

        public List<contactDepartment> getContactDepartment(string xmlPath)
        {
            XDocument xContactDocument = XDocument.Load(xmlPath);
            contactDepartment objContactDepartment = new contactDepartment();

            List<contactDepartment> lstContactDepartment
           = (from _contactDepartment in xContactDocument.Element("contact").Elements("department")
              select new contactDepartment
              {
                  seq = Convert.ToInt16(_contactDepartment.Element("seq").Value),
                  code = _contactDepartment.Element("code").Value,
                  name = _contactDepartment.Element("name").Value,
                  mailto = _contactDepartment.Element("mailto").Value,
                  mailcc = _contactDepartment.Element("mailcc").Value,
                  casetype = _contactDepartment.Element("casetype").Value,
              }).ToList();

            return lstContactDepartment;
        }

        public string getContactDepartmentByCaseType(string xmlPath, string casetypecode)
        {
            if (casetypecode == null)
            {
                return string.Empty;
            }

            string departmentCode = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlPath);
            XmlNodeList nodeList = doc.SelectNodes("/contact/department");

            foreach (XmlNode node in nodeList)
            {
                if (node["casetype"].InnerText.ToLower().Contains("|" +  casetypecode.ToLower() + "|"))
                {
                    return departmentCode = node["code"].InnerText;
                }
            }
            return departmentCode;

        }

        public string getContactDepartmentNameByCode(string xmlPath, string departmentcode)
        {
            if (departmentcode == null)
            {
                return string.Empty;
            }

            string departmentName = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlPath);
            XmlNodeList nodeList = doc.SelectNodes("/contact/department");

            foreach (XmlNode node in nodeList)
            {
                if (node["departmentcode"].InnerText.ToLower().Contains(departmentcode.ToLower()))
                {
                    return departmentName = node["name"].InnerText;
                }
            }
            return departmentName;

        }

        public string getContactDepartmentMailByCode(string xmlPath, string departmentcode, string mailtype) //mailtype mailto or mailcc
        { 
            string departmentMail = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlPath);
            XmlNodeList nodeList = doc.SelectNodes("/contact/department");
           
            foreach (XmlNode node in nodeList)
            {
                if (node["code"].InnerText.ToLower().Contains(departmentcode.ToLower()))
                {
                    if (mailtype == "mailto")
                    {
                        return departmentMail = node["mailto"].InnerText;
                    }
                    else
                    {
                        return departmentMail = node["mailcc"].InnerText;
                    }
                }
            }
            return departmentMail;

        }

        public string getContactDepartmentMailByCaseType(string xmlPath, string casetypecode, string mailtype) //mailtype mailto or mailcc
        {
            string departmentMail = string.Empty; 

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlPath);
            XmlNodeList nodeList = doc.SelectNodes("/contact/department");

            foreach (XmlNode node in nodeList)
            {
                if (node["casetype"].InnerText.ToLower().Contains(casetypecode.ToLower()))
                {
                    if (mailtype == "mailto")
                    {
                        return departmentMail = node["mailto"].InnerText;
                    }
                    else
                    {
                        return departmentMail = node["mailcc"].InnerText;
                    }
                }
            }

            return departmentMail;

        }
        /*********************************************************************************************************************************************/

    }

    public class CasetypeViewModel
    {
        public List<CaseType> list_node { get; set; }
        public int? selectedID { get; set; }

        // append by suksa
        public List<contactDepartment> contactDepartment { get; set; }
        public string selectedDepartmentCode { get; set; }
    }

    public class SourcetypeViewModel
    {
        public  List<string> sourceType  { get; set; }        
    }
}