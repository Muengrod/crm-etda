//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NEWCRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CaseIssueID
    {
        public int issid { get; set; }
        public Nullable<int> casid { get; set; }
        public Nullable<int> casidlevel { get; set; }
        public string cascreate { get; set; }
        public Nullable<int> issno { get; set; }
        public Nullable<System.DateTime> isscreatedate { get; set; }
    }
}
