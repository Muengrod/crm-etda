/*
Navicat SQL Server Data Transfer

Source Server         : 172.22.138.146
Source Server Version : 110000
Source Host           : 172.22.138.146:1433
Source Database       : crmedta_dev
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 110000
File Encoding         : 65001

Date: 2016-12-15 15:37:30
*/


-- ----------------------------
-- Table structure for [dbo].[ContactDepartmentEmail]
-- ----------------------------
DROP TABLE [dbo].[ContactDepartmentEmail]
GO
CREATE TABLE [dbo].[ContactDepartmentEmail] (
[seq] int NOT NULL ,
[code] nvarchar(50) NULL ,
[name] nvarchar(255) NULL ,
[mailto] nvarchar(255) NULL ,
[mailcc] nvarchar(255) NULL ,
[casetype] nvarchar(500) NULL 
)


GO

-- ----------------------------
-- Records of ContactDepartmentEmail
-- ----------------------------
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'1', N'ETDA', N'ETDA', N'occ@etda.or.th', N'mict1212@oto.samartcorp.com,mict1212occ@gmail.com', N'|8|9|18|19|22|25|28|162|163|164|219|222|223|224|169|170|171|172|173|204|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'2', N'PACC', N'ปท.', N'csoc@mict.go.th, itcrime@mict.go.th', N'occ@etda.or.th,mict1212@oto.samartcorp.com,mict1212occ@gmail.com', N'|179|180|181|202|203|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'3', N'TCSD', N'ปอท.', N'occ@etda.or.th', N'mict1212@oto.samartcorp.com,mict1212occ@gmail.com', N'|145|147|148|149|150|183|184|186|187|188|191|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'4', N'FDA', N'อย.', N'occ@etda.or.th', N'mict1212@oto.samartcorp.com,mict1212occ@gmail.com', N'|151|185|201|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'5', N'ONCB', N'ปปส', null, null, N'|189|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'6', N'ECD', N'ปอศ', null, null, N'|190|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'7', N'IPTHAI', N'กรมทรัพย์สินทางปัญญา', null, null, N'|192|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'8', N'ONAB', N'สำนักงานพระพุทธศาสนา', null, null, N'|193|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'9', N'TOUR', N'กรมการท่องเที่ยว', null, null, N'|194|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'10', N'DCY', N'กองคุ้มครองเด็กและเยาวชน', null, null, N'|195|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'11', N'MOL', N'กระทรวงแรงงาน', null, null, N'|196|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'12', N'MOE', N'กระทรวงศึกษาธิการ', null, null, N'|197|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'13', N'ThaiCERT', N'ThaiCERT', N'report@thaicert.or.th', N'occ@etda.or.th ,mict1212@oto.samartcorp.com,mict1212occ@gmail.com', N'|153|154|155|156|157|158|159|160|161|216|217|218|198|199|200|205|206|207|208|209|210|211|212|213|214|215|');
GO
INSERT INTO [dbo].[ContactDepartmentEmail] ([seq], [code], [name], [mailto], [mailcc], [casetype]) VALUES (N'14', N'OTO', N'OTO', null, null, N'|174|175|176|177|178|189|190|192|193|194|195|196|197|225|');
GO

-- ----------------------------
-- Indexes structure for table ContactDepartmentEmail
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table [dbo].[ContactDepartmentEmail]
-- ----------------------------
ALTER TABLE [dbo].[ContactDepartmentEmail] ADD PRIMARY KEY ([seq])
GO
