﻿using NEWCRM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using NEWCRM.Common;
namespace NEWCRM.Controllers
{
    [LoginExpireAttribute]
    public class CasetypeController : Controller
    {

        #region bom


        public ActionResult GetSourceType()
        {

            var model = new SourcetypeViewModel();
            model.sourceType = ConfigurationManager.AppSettings["CASE_SOURCE_TYPE"].ToString().Split('|').ToList();
            return PartialView("SourceTypeNode", model);
        }

        public ActionResult getCaseTypeNodeSelectionRepCase(int id, int? selectedID = null)
        {
            var list_casetype = Common.AppUtils.Session.CaseTypes;
            var nodes = from ct in list_casetype
                        where ct.catParrentID == id
                        orderby ct.catName
                        select ct;

            var model = new CasetypeViewModel();
            model.list_node = nodes.ToList();
            model.selectedID = selectedID;

            if (model.list_node.Count() > 0)
            {
                if (model.list_node[0].catParrentID > 0)
                {
                    ViewData["node_label"] = "ประเภทเรื่องรอง";
                }
                else
                {
                    ViewData["node_label"] = "ประเภทเรื่องหลัก";
                }

                ViewData["ct_level"] = model.list_node[0].catLevel;
                ViewData["node_id"] = string.Format("ctnode_{0}", id);
            }

            return PartialView("CaseTypeNodeSelectionRepCase", model);
        }

    
    #endregion

    //
    // GET: /Casetype/
    public ActionResult getCaseTypeNodeSelection(int id, int? selectedID = null)
        {
            var list_casetype = Common.AppUtils.Session.CaseTypes;
            var nodes = from ct in list_casetype
                       where ct.catParrentID == id && ct.catActive==true
                       orderby ct.catOrder
                       select ct;

            var model = new CasetypeViewModel();
            model.list_node = nodes.ToList();
            model.selectedID = selectedID;

            if (model.list_node.Count() > 0)
            {
                if (model.list_node[0].catParrentID > 0)
                {
                    ViewData["node_label"] = "ประเภทเรื่องรอง";
                }
                else
                {
                    ViewData["node_label"] = "ประเภทเรื่องหลัก";
                }

                ViewData["ct_level"] = model.list_node[0].catLevel;
                ViewData["node_id"] = string.Format("ctnode_{0}", id);
            }

            return PartialView("CaseTypeNodeSelection", model);
        }

   #region SuksaN

    public string getContactDepartmentByCaseType(string id)
    {
        var casetyperepository = new CasetypeRepository();
        string ContactDepartment = casetyperepository.getContactDepartmentByCaseType(Server.MapPath("~/ContactDepartmentEmail.xml"), id);
        return ContactDepartment;
    }

    public string getContactDepartmentNameByCode(string departmentcode)
    {
        var casetyperepository = new CasetypeRepository();
        string DepartmentName = casetyperepository.getContactDepartmentByCaseType(Server.MapPath("~/ContactDepartmentEmail.xml"), departmentcode);
        return DepartmentName;
    }

    public string getContactDepartmentMailByCode(string departmentcode, string mailtype)
    {
        var casetyperepository = new CasetypeRepository();
        string DepartmentMail = casetyperepository.getContactDepartmentMailByCode(Server.MapPath("~/ContactDepartmentEmail.xml"), departmentcode, mailtype);
        return DepartmentMail;
    }

    public ActionResult getContactDepartment()
    {
        var model = new CasetypeViewModel();
        var casetyperepository = new CasetypeRepository();
        List<contactDepartment> ContactDepartment = casetyperepository.getContactDepartment(Server.MapPath("~/ContactDepartmentEmail.xml"));
        model.contactDepartment = ContactDepartment;
        return PartialView("ContactDepartment", model);
    }

    public ActionResult getContactDepartmentNew()
    {
        var model = new CaseViewModelLocalization();
        var casetyperepository = new CasetypeRepository();
        List<contactDepartment> ContactDepartment = casetyperepository.getContactDepartment(Server.MapPath("~/ContactDepartmentEmail.xml"));
        return PartialView("ContactDepartmentEdit", model);
    }

    public ActionResult getContactDepartmentEdit(string departmentcode = null)
    {
        var model = new CaseViewModelLocalization();
        var casetyperepository = new CasetypeRepository();
        List<contactDepartment> ContactDepartment = casetyperepository.getContactDepartment(Server.MapPath("~/ContactDepartmentEmail.xml"));
        model.contactDepartment = ContactDepartment;
        model.selectedDepartmentCode = departmentcode;
        return PartialView("ContactDepartmentEdit", model);
    }

    public ActionResult getContactDepartmentEmail(string departmentcode = null)
    {
        var model = new CaseModels();
        var casetyperepository = new CasetypeRepository();
        List<contactDepartment> ContactDepartment = casetyperepository.getContactDepartment(Server.MapPath("~/ContactDepartmentEmail.xml"));
        model.contactDepartment = ContactDepartment;
        model.selectedDepartmentCode = departmentcode;
        return PartialView("ContactDepartmentEmail", model);
    }     
        #endregion

    }
    
}
