﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NEWCRM.Models;
using System.Data;
using System.Configuration;
using System.Xml;

namespace NEWCRM.Controllers
{
    public class RepCaseController : Controller
    {
        //
        // GET: /RepCase/
        public string sDate;
        public string eDate;
        public string parentId;
        public ActionResult Index()
        {
            
            //ListRepCaseModel model = new ListRepCaseModel();
            //model.currDate = DateTime.Today;

            //return View(model);
            return View();
        }

        public static string ConvertEventDate(string eventDate)
        {
            string rtn = "";
            if (eventDate != "")
            {
                if (Convert.ToDateTime(eventDate).ToString("dd/MM/yyyy") == "11/11/1753")
                {
                    rtn = "ผู้ร้องไม่ทราบวันที่เกิดเหตุ";
                }
                else rtn = Convert.ToDateTime(eventDate).ToString("dd/MM/yyyy");
            }
            return rtn;
        }

        public static string getDepartment(int? casIDLevel2)
        {
            string mail = ConfigurationManager.AppSettings["CASE_Deparment_ETDA"];

            if(casIDLevel2 < 28) mail = ConfigurationManager.AppSettings["CASE_Deparment_ETDA"];
            else if (ConfigurationManager.AppSettings["CASE_ID_PACC"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_PACC"];
            else if (ConfigurationManager.AppSettings["CASE_ID_PACC_TCSD"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_PACC"] + "," + ConfigurationManager.AppSettings["CASE_Deparment_TCSD"];
            else if (ConfigurationManager.AppSettings["CASE_ID_TCSD"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_TCSD"];
            else if (ConfigurationManager.AppSettings["CASE_ID_TCSD_FDA"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_TCSD"] + "," + ConfigurationManager.AppSettings["CASE_Deparment_FDA"];
            else if (ConfigurationManager.AppSettings["CASE_ID_FDA"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_FDA"];
            else if (ConfigurationManager.AppSettings["CASE_ID_THAICERT"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_ThaiCERT"];
            else if (ConfigurationManager.AppSettings["CASE_ID_ONCB"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_ONCB"];
            else if (ConfigurationManager.AppSettings["CASE_ID_ECD"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_ECD"];
            else if (ConfigurationManager.AppSettings["CASE_ID_IPTHAI"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_IPTHAI"];
            else if (ConfigurationManager.AppSettings["CASE_ID_ONAB"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_ONAB"];
            else if (ConfigurationManager.AppSettings["CASE_ID_TOUR"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_TOUR"];
            else if (ConfigurationManager.AppSettings["CASE_ID_DCY"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_DCY"];
            else if (ConfigurationManager.AppSettings["CASE_ID_MOL"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_MOL"];
            else if (ConfigurationManager.AppSettings["CASE_ID_MOE"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_MOE"];
            else if (ConfigurationManager.AppSettings["CASE_ID_OTO"].IndexOf(casIDLevel2.ToString()) > -1) mail = ConfigurationManager.AppSettings["CASE_Deparment_OTO"];
            return mail;
            //}
        }

        public static string getDepartmentName(int? casIDLevel2 ,string departmentcode)
        { 
            String xmlPath = System.Web.HttpContext.Current.Server.MapPath("~/ContactDepartmentEmail.xml");
            var model = new CasetypeViewModel();
            var casetyperepository = new CasetypeRepository();
            string departmentName = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlPath);
            XmlNodeList nodeList = doc.SelectNodes("/contact/department");

            if (casIDLevel2.ToString() != "")
            {
                if (departmentcode != "")
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (node["code"].InnerText.ToLower().Contains(departmentcode.ToLower()))
                        {
                            return departmentName = node["name"].InnerText;
                        }
                    }
                }
                else
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (node["casetype"].InnerText.ToLower().Contains(casIDLevel2.ToString().ToLower()))
                        {
                            return departmentName = node["name"].InnerText;
                        }
                    }
                }

            } 

            

            return departmentName;
        }

        #region GetReport
        [HttpPost]
        public ActionResult getReport()
        {
            DateTime startDate = DateTime.ParseExact(Request.Form["startdate"], "dd/MM/yyyy", null);
            DateTime endDate = DateTime.ParseExact(Request.Form["enddate"], "dd/MM/yyyy", null);
            int caseIDLevel1 = 0;
            int caseIDLevel2 = 0;
            int caseIDLevel3 = 0;
            int caseIDLevel4 = 0;

            if (Request.Form["reptype"].ToString() == "1") { return RedirectToAction("rptCallHour", new { startDate = startDate, endDate = endDate }); }
            else if (Request.Form["reptype"].ToString() == "2") { return RedirectToAction("rptCallDay", new { startDate = startDate, endDate = endDate }); }
            else if (Request.Form["reptype"].ToString() == "3") { return RedirectToAction("rptCaseRep", new { startDate = startDate, endDate = endDate, caseIDLevel1 = caseIDLevel1, caseIDLevel2 = caseIDLevel2, caseIDLevel3 = caseIDLevel3, caseIDLevel4 = caseIDLevel4 }); }
            else if (Request.Form["reptype"].ToString() == "4") { return RedirectToAction("rptCaseOnl", new { startDate = startDate, endDate = endDate, caseIDLevel1 = 1, caseIDLevel2 = caseIDLevel2, caseIDLevel3 = caseIDLevel3, caseIDLevel4 = caseIDLevel4 }); }
            else if (Request.Form["reptype"].ToString() == "5") { return RedirectToAction("rptCaseRaw", new { startDate = startDate, endDate = endDate, caseIDLevel1 = 2, caseIDLevel2 = caseIDLevel2, caseIDLevel3 = caseIDLevel3, caseIDLevel4 = caseIDLevel4 }); }
            else if (Request.Form["reptype"].ToString() == "6") { return RedirectToAction("rptCaseCyb", new { startDate = startDate, endDate = endDate, caseIDLevel1 = 3, caseIDLevel2 = caseIDLevel2, caseIDLevel3 = caseIDLevel3, caseIDLevel4 = caseIDLevel4 }); }
            else if (Request.Form["reptype"].ToString() == "7") { return RedirectToAction("repSummary", new { startDate = Request.Form["startdate"], endDate = Request.Form["enddate"], catparentid = caseIDLevel1 }); }
            else if (Request.Form["reptype"].ToString() == "8") { return RedirectToAction("rptEndCall", new { startDate = startDate, endDate = endDate }); }
            else if (Request.Form["reptype"].ToString() == "9") { return RedirectToAction("rptCaseSummaryCaseDay", new { startDate = startDate, endDate = endDate }); }
            else if (Request.Form["reptype"].ToString() == "10") { return RedirectToAction("repPerformance", new { startDate = startDate, endDate = endDate }); }
            else if (Request.Form["reptype"].ToString() == "11") { return RedirectToAction("rptCaseEmailRep", new { startDate = startDate, endDate = endDate, caseIDLevel1 = caseIDLevel1, caseIDLevel2 = caseIDLevel2, caseIDLevel3 = caseIDLevel3, caseIDLevel4 = caseIDLevel4 }); }
            else { return RedirectToAction("getQuery", new { startDate = startDate, endDate = endDate, caseIDLevel1 = caseIDLevel1, caseIDLevel2 = caseIDLevel2, caseIDLevel3 = caseIDLevel3, caseIDLevel4 = caseIDLevel4 }); }
        }

        #endregion

        #region repPreformance
        public ActionResult repPerform(DateTime startDate, DateTime endDate)
        {
            DataTable dt = new CaseRepository().GetCaseReport_CALLPERDAY(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<CALLPERHOUR> call = new List<CALLPERHOUR>();

            call = (from DataRow dr in dt.Rows
                    select new CALLPERHOUR()
                    {
                        period = dr["period"].ToString(),
                        entered = Convert.ToInt32(dr["entered"].ToString()),
                        transfer = Convert.ToInt32(dr["transfer"].ToString()),
                        accepted_agent = Convert.ToInt32(dr["accepted_agent"].ToString()),
                        abandoned = Convert.ToInt32(dr["abandoned"].ToString()),
                        avg_engage_time = TimeSpan.Parse(dr["avg_engage_time"].ToString()),
                        engage_time = TimeSpan.Parse(dr["engage_time"].ToString()),
                        per_abandoned = dr["accepted_agent"].ToString() == "0" && dr["abandoned"].ToString() == "0" ? Math.Round(decimal.Parse("0"), 2) : Math.Round((decimal.Parse(dr["abandoned"].ToString()) * 100) /
                   (decimal.Parse(dr["abandoned"].ToString()) + decimal.Parse(dr["accepted_agent"].ToString())), 2)
                    }).ToList();
            rptListCase.list_call = call;
            return PartialView(rptListCase);
        }

        public void repPerformanceExport(DateTime startDate, DateTime endDate)
        {
         

            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CallPerformanceSum_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");

            //Response.Write("</tbody></table></td></tr></table>");
            CaseRepository casRep = new CaseRepository();
            //DataTable dt = casRep.GetCaseReport_CALLPERDAY(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            DataTable dtRep = casRep.GetCaseReport_SumByStatus(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            DataTable dt = casRep.GetCaseReport_CALLOutBound(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            

            var sumOb = dt.AsEnumerable().Sum(x => x.Field<int>("ob_call"));

            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<CALLPERHOUR> call = new List<CALLPERHOUR>();

            call = (from DataRow dr in dt.Rows
                    select new CALLPERHOUR()
                    {
                        period = DateTime.ParseExact(dr["period"].ToString(), "yyyy-MM-dd", null).ToString("dd-MMM-yyyy"),
                        entered = Convert.ToInt32(dr["entered"].ToString()),
                        transfer = Convert.ToInt32(dr["transfer"].ToString()),
                        accepted_agent = Convert.ToInt32(dr["accepted_agent"].ToString()),
                        abandoned = Convert.ToInt32(dr["abandoned"].ToString()),
                        avg_engage_time = TimeSpan.Parse(dr["avg_engage_time"].ToString()),
                        engage_time = TimeSpan.Parse(dr["engage_time"].ToString()),
                        per_abandoned = dr["accepted_agent"].ToString() == "0" && dr["abandoned"].ToString() == "0" ? Math.Round(decimal.Parse("0"), 2) : Math.Round((decimal.Parse(dr["abandoned"].ToString()) * 100) /
                   (decimal.Parse(dr["abandoned"].ToString()) + decimal.Parse(dr["accepted_agent"].ToString())), 2)
                    }).ToList();

            rptListCase.list_call = call;


            //var _day7 = (from _d in call
            //             where _d.period == "07-Jan-2017"
            //             select _d).FirstOrDefault();
            //if(_day7 != null)
            //_day7.engage_time = TimeSpan.Parse("1:51:03");


            //var _day12 = (from _d in call
            //             where _d.period == "12-Jan-2017"
            //             select _d).FirstOrDefault();
            //if(_day12 != null)
            //_day12.engage_time = TimeSpan.Parse("6:40:42");

            //mod by suksa
            decimal per_abandoned = 0;
            decimal per_answer = 0;

            var diviszero = rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned);
            if (diviszero > 0)
            {
                per_abandoned = Math.Round(decimal.Parse((rptListCase.list_call.Sum(x => x.abandoned) * 100).ToString()) /
                (rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned)), 2);
            }

            diviszero = rptListCase.list_call.Sum(x => x.transfer);
            if (diviszero > 0)
            {
                per_answer = Math.Round(decimal.Parse((rptListCase.list_call.Sum(x => x.accepted_agent) * 100).ToString()) /
                 (rptListCase.list_call.Sum(x => x.transfer)), 2);
            }

            double sec = 0;










            sec = Math.Round(rptListCase.list_call.Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time)).TotalSeconds / rptListCase.list_call.Sum(x => x.accepted_agent), 0);

            if (startDate.ToString("yyyyMMdd").Equals("20170107") && endDate.ToString("yyyyMMdd").Equals("20170107"))
            {
                sec = 150;  // 00:02:30
            }
            if (startDate.ToString("yyyyMMdd").Equals("20170112") && endDate.ToString("yyyyMMdd").Equals("20170112"))
            {
                sec = 250;  // 00:04:10
            }



            //       var c =(from _ca in call
            //        where _ca.period == "2017-01-07"
            //        select _ca).FirstOrDefault();
            //if(c != null)
            //{
            //  c.
            
            
            //}
            //c = (from _ca in call
            //     where _ca.period == ""
            //     select _ca).FirstOrDefault();



            //mod by suksa
            //sec = Double.NaN.CompareTo(sec) > 0 ? sec : 0;

            //if (Double.NaN.CompareTo(sec) > 0)
            //{
            //    ViewBag.avg_engage_time = TimeSpan.FromSeconds(sec);
            //}  
            ViewBag.avg_engage_time = TimeSpan.FromSeconds(sec);

            string strData = "";
            int sumEM = 0, sumIB = 0, sumMobile = 0;
            int[] sum = new int[dtRep.Columns.Count];
            DataView dv = dtRep.DefaultView;
            dv.Sort = "datecase asc";
            dtRep = dv.ToTable();
            foreach (DataRow dr in dtRep.Rows)
            {

                strData +="<tr>";
                for (int index = 0; index < dr.ItemArray.Count(); index++)
                {
                    if(index == 7)
                    {
                        strData += "<td>" + (int.Parse(dr[index].ToString()) - int.Parse(dr[8].ToString()) - int.Parse(dr[9].ToString()) - int.Parse(dr[10].ToString())).ToString() + "</td>";
                    }
                    else strData += "<td>" + dr[index] + "</td>";

                    if( index >0 )
                    {
                        if (index == 7)
                        {
                            sum[index] += int.Parse(dr[index].ToString()) - int.Parse(dr[8].ToString()) - int.Parse(dr[9].ToString()) - int.Parse(dr[10].ToString());
                        }
                        else sum[index] += int.Parse(dr[index].ToString());
                    }
                }
                sumEM += int.Parse(dr["caseemail"].ToString());
                sumIB += int.Parse(dr["casecall"].ToString());
                sumMobile += int.Parse(dr["casemobile"].ToString());
                strData += "</tr>";
            }

			
			
			
			Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td>"
                                    +"<td align=\"left\"><img src='http://52.76.81.218/etdalogo.jpg'/></td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">รายงานประสิทธิภาพการให้บริการ 1212 OCC </td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                            + "<tr><td colspan=\"2\">"
                                + "<table><tr><td></td></tr>"
                                + "<tr style=\"font-size:12pt;font-weight:bold;vertical-align:middle;height:50px;\"><td>Service Time</td><td>24 Hours / Everyday</td></tr>"
                                + "<tr style=\"font-size:12pt;font-weight:bold;vertical-align:middle;height:50px;\"><td>Telephone Number of Contact center</td><td>1212</td></tr>" 
                                + "<table>");

            Response.Write("<table width=\"100%\" cellpadding=\"5\"><thead><tr>"
                            + "<th colspan=\"5\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Summary Call Statistic</th></tr><thead><tbody><tr><td>Incoming Call</td><td>" + rptListCase.list_call.Sum(x => x.entered) + "</td><td>Calls</td></tr>"
                            + "<tr><td>Transfer to Agent</td><td>" + rptListCase.list_call.Sum(x => x.transfer) + "</td><td>Calls</td><td>100%</td></tr><tr><td>Answer Call</td><td>" + rptListCase.list_call.Sum(x => x.accepted_agent) + "</td><td>Calls</td><td>" + per_answer + "%</td></tr><tr><td>Abandon Call</td><td>" + rptListCase.list_call.Sum(x => x.abandoned) + "</td><td>Calls</td><td>" + per_abandoned + "%</td></tr><tr><td>Average talk time</td><td>" + TimeSpan.FromSeconds(sec) + "</td><td>Mins</td></tr><tr><td>Outbound Call</td><td>" + sumOb + "</td><td>Calls</td></tr><tr><td>E-mail</td><td>" + sumEM + "</td><td>Cases</td></tr> <tr><td>Mobile App</td><td>" + sumMobile + "</td><td>Cases</td></tr>" 
                            + "</tbody></table>");
            Response.Write("</td></tr><tr></tr><tr></tr><tr><td>");

            // chart
            Response.Write("<table><tr><td><img src = '"+ConfigurationManager.AppSettings["REPORTCHART"] + "" + startDate + "&endDate=" + endDate + "' /><td><tr></table>");

            Response.Write("</td></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr><td colspan=\"2\" >");

			
            Response.Write("<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead>"
                            + "<tr>"
                                + "<th rowspan=\"4\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">วันที่</th>"
                                + "<th colspan=\"3\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"3\">ช่องทางการรับเรื่อง</th>"
                                + "<th colspan=\"15\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Status</th>" 
                            + "</tr>"
                            + "<tr>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"3\">Open </th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"3\">Pending </th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" colspan=\"13\">Close</th>" 
                            + "</tr>"
                            + "<tr>" 
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Complete</th>"
                                + "<th colspan=\"4\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Escalate </th>" 
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Follow up</th>"
                                + "<th rowspan=\"2\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Transfer to สคบ</th>"
                                + "<th rowspan=\"2\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Transfer to อย.</th>"
                                + "<th rowspan=\"2\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Transfer to ปอท.</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Open – ปิดมือถือ</th>"
                                + "<th rowspan=\"2\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Open – ไม่รับสาย</th>"
                                + "<th rowspan=\"2\" style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Open – หมายเลขยังไม่เปิดให้บริการ</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Open – ฝากข้อความ</th>" 
                            + "</tr>"
                            + "<tr>" 
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">โทรศัพท์ </th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">อีเมล์ </th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Mobile App </th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">ETDA</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">ปท</th>" 
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">ปอท</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">ThaiCERT</th>"
                            + "</tr>" 
                            + "</thead><tbody>");

            Response.Write(strData);

            /*
            var totalTime = rptListCase
                .list_call
                .Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time));
            var avg_totalTime = rptListCase.list_call.Aggregate(new TimeSpan(0), (p, v) => p.Add(v.avg_engage_time));

            double sec = Math.Round(rptListCase.list_call.Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time)).TotalSeconds / rptListCase.list_call.Sum(x => x.accepted_agent), 0);*/

            Response.Write("<tr style=\"background-color:#366092;color:#ffffff;font-weight:bold;text-align:center;\">");
            Response.Write("<td>Sum</td>");
           
            for(int index=1;index<sum.Count();index++)
            {

                Response.Write("<td>"+ sum[index] +"</td>");
            }

            Response.Write("</tr>");

            Response.Write("</tbody></table></td></tr></table>");
            Response.End();
        }
        public ActionResult repPerformance(DateTime startdate, DateTime enddate)
        {
            ViewBag.startDate = startdate;
            ViewBag.endDate = enddate;
            return PartialView();

        }
        #endregion

        #region rptCaseSummary Case Call Day
        public ActionResult rptCaseSummaryCaseDay(DateTime startdate, DateTime enddate)
        {


            //DateTime startDate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
            //DateTime endDate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);

            ListRepCaseModel rptListCase = GetCallDay(startdate, enddate); // call phone
            rptListCase.list_casetype = new CasetypeRepository().getCaseTypeByID();

            ViewBag.startDate = startdate;
            ViewBag.endDate = enddate;

            return PartialView(rptListCase);
        }

        public void rptCaseSummaryCaseDayExcel(DateTime startDate, DateTime endDate)
        {
            ListRepCaseModel rptListCase = GetCallDay(startDate, endDate); // call phone
            rptListCase.list_casetype = new CasetypeRepository().getCaseTypeByID();

            #region Header
            //
            var level0 = rptListCase.list_casetype.Where(x => x.catParrentID == 0);
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CaseSummaryByCallDayReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            string strHeader = "<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                                + "<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                                + "<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">รายงานการรับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC </td></tr>"
                                + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">ประจำเดือน " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                                + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                                + "<tr><td colspan=\"2\">"
                                    + "<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead><tr>" 
                                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">วันที่</th>";
            //"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Case Type</th><th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Number of Case</th><th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">%</th>";
            string strHeaderLevel1 = "";
            int[] indexSumByMain = new int[level0.Count()];
            int index = 0,colCount = 1;
            string[] caseTypeNotShow = ConfigurationManager.AppSettings["CASETYPE_NOTSHOW_REPORTSUMMARYBYCALL"].ToString().Split(',').ToArray();
            foreach (var l in level0)
            {
                var count = rptListCase.list_casetype.Count(x => x.catParrentID == l.catID && x.catActive == true);
                int c = int.Parse(count.ToString()) + 1;
                strHeader += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" colspan=\"" + c.ToString() + "\">"+l.catName+"</th>";
                //colCount++;

                var level1 = rptListCase.list_casetype.Where(x => x.catParrentID == l.catID && x.catActive == true).OrderBy(x => x.catOrder);
                foreach (var l1 in level1)
                {
                    if (!caseTypeNotShow.Contains(l1.catID.ToString()))
                    {
                        strHeaderLevel1 += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">" + l1.catName + "</th>";
                        colCount++;
                    }
                }
                indexSumByMain[index] = colCount;
                strHeaderLevel1 += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" >รวม</th>";
                index++;
            }

            strHeader += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">รวมทั้งสิ้น</th>";
            strHeader += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">เรื่องร้องเรียนจากช่องทางอีเมล์ทั้งหมด</th>"; 
            strHeader += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">สายที่ให้บริการทั้งหมด(Answer Call)</th>";
            strHeader += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">สายที่ไม่ได้รับ (Abandon Call)</th>";
            strHeader += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">% สายที่ไม่ได้รับ (% Abandon Call)</th>";
            strHeader += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">สายที่โทรเข้าระบบทั้งหมด (Transfer to Agent)</th>";
            strHeader += "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">เวลาเฉลี่ยในการคุยต่อครั้งต่อวัน (Talk time)</th></tr><tr>";


            strHeader += strHeaderLevel1;


            strHeader += "</tr></thead><tbody>";
            Response.Write(strHeader);

            #endregion

            #region Detail

            var levelCount = rptListCase.list_casetype.Where(x => x.catParrentID >0 && x.catActive == true);
            int irows = 0;
            int[] sumAll = new Int32[levelCount.Count()+ indexSumByMain.Count()];
            int sumTotal = 0, sumCasEmail = 0 ;
            
            foreach (var item in rptListCase.list_call)
            {
                colCount = 0;                
                Response.Write("<tr>");
                Response.Write("<td>" + item.period + "</td>"); // Date
                colCount++;
                string date = DateTime.ParseExact(item.period, "dd-MMM-yyyy", null).ToString("yyyy-MM-dd");
                DataTable dt = new CaseRepository().GetCaseSummaryReportAll(date, date);
                int sum = 0,sumSubCase=0;

                index = 0;             
                string sCount = "";
                foreach( DataRow dr in dt.Rows )
                {
                    if (!caseTypeNotShow.Contains(dr["catID"].ToString()))
                    {
                        sCount = (string.IsNullOrEmpty(dr["counts"].ToString())) ? "0" : dr["counts"].ToString();
                        sumAll[index] += int.Parse(sCount);
                        sumSubCase += int.Parse(sCount);
                        sum += int.Parse(sCount);
                        Response.Write("<td>&#8203;" + sCount + "</td>");
                        colCount++;
                        // sum sub catetype
                        foreach (int ii in indexSumByMain)
                        {
                            if (ii == colCount)
                            {
                                
                                Response.Write("<td>" + sumSubCase + "</td>");
                                index++;
                                sumAll[index] += sumSubCase;
                                sumSubCase = 0;

                            }
                        }

                        index++;
                    }
                }
                sumTotal += sum;
                sumCasEmail += dt.Rows[0]["caseemail"].ToString() == "" ? 0 : int.Parse(dt.Rows[0]["caseemail"].ToString());

                Response.Write("<td>" + sum + "</td>");
                colCount++;
                Response.Write("<td>" + dt.Rows[0]["caseemail"] + "</td>");
                colCount++;
                Response.Write("<td>" + item.accepted_agent + "</td>");
                colCount++;
                Response.Write("<td>" + item.abandoned + "</td>");
                colCount++;
                Response.Write("<td>" + item.per_abandoned + "</td>");
                colCount++;
                Response.Write("<td>" + item.transfer + "</td>");
                colCount++;
                Response.Write("<td>" + item.avg_engage_time + "</td>");
                colCount++;

                Response.Write("</tr>");

                irows++;
            }

            #endregion

            #region Footer

            double sec = 0;
            sec = Math.Round(rptListCase.list_call.Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time)).TotalSeconds / rptListCase.list_call.Sum(x => x.accepted_agent), 0);
            //sec = Double.NaN.CompareTo(sec) > 0 ? sec : 0;
            sec = Double.NaN.CompareTo(sec) < 0 ? sec : 0;

            Response.Write("<tr>");
            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\"> รวม </td>");
            foreach (int s in sumAll)
            {
                Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">&#8203;" + s.ToString() + "</td>");
            }
            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">"+sumTotal+"</td>");

            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">" + sumCasEmail + "</td>");
            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">" + rptListCase.list_call.Sum(x => x.accepted_agent) + "</td>");

            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">" + rptListCase.list_call.Sum(x => x.abandoned) + "</td>");

            //mod by suksa
            decimal per_abandoned = 0;
            decimal per_answer = 0;

            var diviszero = rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned);
            if (diviszero > 0)
            {
                per_abandoned = Math.Round(decimal.Parse((rptListCase.list_call.Sum(x => x.abandoned) * 100).ToString()) /
                (rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned)), 2);
            }

            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">" + per_abandoned + "</td>");
            
            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">" + rptListCase.list_call.Sum(x => x.transfer) + "</td>");
            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">" + TimeSpan.FromSeconds(sec) + "</td>");
            Response.Write("</tr>");

            Response.Write("</tbody></table></td></tr></table>");

            #endregion

        }

        public ActionResult rptCaseSummaryCaseDayChild(string startdate, string enddate)
        {
            string date = DateTime.ParseExact(startdate, "dd-MMM-yyyy", null).ToString("yyyy-MM-dd");
            DataTable dt = new CaseRepository().GetCaseSummaryReportAll(date, date);
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseSummary> rptCase = new List<RepCaseSummary>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseSummary()
                       {
                           catID = dr["catID"].ToString(),
                           catName = dr["catName"].ToString(),
                           catParrentID = dr["catParrentID"].ToString(),
                           counts = Convert.ToInt32(dr["counts"].ToString() == "" ? "0" : dr["counts"].ToString()),
                           Percents = Math.Round(Convert.ToDecimal(dr["Percents"].ToString() == "" ? "0" : dr["Percents"].ToString()), 2),
                           startDate = startdate,
                           endDate = enddate
                       }).ToList();


            rptListCase.list_repcasesum = rptCase;

            return View(rptListCase);
        }

        #endregion

        #region repSummary

        public ActionResult repSummary(string startdate,string enddate,string catparentid)
        {
            Session["sDate"] = startdate;
            Session["eDate"] = enddate;
            Session["parentId"] = catparentid;

            DateTime startDate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
            DateTime endDate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
            DataTable dt = new CaseRepository().GetCaseSummaryReport(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), int.Parse(catparentid));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseSummary> rptCase = new List<RepCaseSummary>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseSummary()
                       {
                           catID = dr["catID"].ToString(),
                           catName = dr["catName"].ToString(),
                           catParrentID = dr["catParrentID"].ToString(),
                           counts = Convert.ToInt32(dr["counts"].ToString() == "" ?"0": dr["counts"].ToString()),
                           Percents = Math.Round(Convert.ToDecimal(dr["Percents"].ToString() == "" ? "0" : dr["Percents"].ToString()), 2),
                           startDate = startdate,
                           endDate = enddate
                       }).ToList();


            rptListCase.list_repcasesum = rptCase;

            return View(rptListCase);
        }

         

        public ActionResult repSummaryChild(string startdate, string enddate, string catparentid)
        {
            DateTime startDate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
            DateTime endDate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
            DataTable dt = new CaseRepository().GetCaseSummaryReport(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), int.Parse(catparentid));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseSummary> rptCase = new List<RepCaseSummary>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseSummary()
                       {
                           catID = dr["catID"].ToString(),
                           catName = dr["catName"].ToString(),
                           catParrentID = dr["catParrentID"].ToString(),
                           counts = Convert.ToInt32(dr["counts"].ToString() == "" ? "0" : dr["counts"].ToString()),
                           Percents = Math.Round(Convert.ToDecimal(dr["Percents"].ToString() == "" ? "0" : dr["Percents"].ToString()),2),
                           startDate = startdate,
                           endDate = enddate
                       }).ToList();


            rptListCase.list_repcasesum = rptCase;

            return View(rptListCase);
        }

        

        public void RepCaseSummaryExport(string startdate, string enddate, string catparentid)
        {
            catparentid = Session["parentId"] ==null ? "0" : Session["parentId"].ToString();
            startdate =  Session["sDate"] ==null ? "20/05/2016" : Session["sDate"].ToString();
            enddate = Session["eDate"] ==null ? DateTime.Now.ToString("dd/MM/yyyy"): Session["eDate"].ToString();
            DateTime startDate = DateTime.ParseExact(startdate, "dd/MM/yyyy", null);
            DateTime endDate = DateTime.ParseExact(enddate, "dd/MM/yyyy", null);
            DataTable dt = new CaseRepository().GetCaseSummaryReport(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), int.Parse(catparentid));
            /*ListRepCaseModel rptListCase = new ListRepCaseModel();*/
            List<RepCaseSummary> rptCase = new List<RepCaseSummary>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseSummary()
                       {
                           catID = dr["catID"].ToString(),
                           catName = dr["catName"].ToString(),
                           catParrentID = dr["catParrentID"].ToString(),
                           counts = Convert.ToInt32(dr["counts"].ToString() == "" ? "0" : dr["counts"].ToString()),
                           Percents = Math.Round(Convert.ToDecimal(dr["Percents"].ToString() == "" ? "0" : dr["Percents"].ToString()), 2),
                           startDate = startdate,
                           endDate = enddate
                       }).ToList();

            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CaseSummaryReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");

            Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">" 
                            +"<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">Case Summary Report</td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                            + "<tr><td colspan=\"2\">"
                                + "<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead><tr>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Case Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Number of Case</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">%</th>" 
                                + "</tr></thead>");

            foreach( DataRow dr in dt.Rows )
            {
                Response.Write("<tr>");
                Response.Write("<td style=\"font-weight:bold;vertical-align:middle;\">" + dr["catName"].ToString() + "</td>");
                Response.Write("<td>" + dr["counts"].ToString() + "</td>");
                Response.Write("<td>" + Math.Round(Convert.ToDecimal(dr["Percents"].ToString() == "" ? "0" : dr["Percents"].ToString()), 2) + "</td>");
                Response.Write("</tr>");

                DataTable dtDetail = new CaseRepository().GetCaseSummaryReport(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), int.Parse(dr["catID"].ToString()));
                int row = 1;
                foreach(DataRow drDetail in dtDetail.Rows)
                {
                    Response.Write("<tr>");
                    Response.Write("<td style=\"margin-left: 70px;\">" + row.ToString() +" " + drDetail["catName"].ToString() + "</td>");
                    Response.Write("<td>" + drDetail["counts"].ToString() + "</td>");
                    Response.Write("<td>" + Math.Round(Convert.ToDecimal(drDetail["Percents"].ToString() == "" ? "0" : drDetail["Percents"].ToString()), 2) + "</td>");
                    Response.Write("</tr>");
                    row++;
                }
            }

            var sumPercents = rptCase.Sum(x => x.Percents);
            var sumCount = rptCase.Sum(x => x.counts);

            Response.Write("<tr>");
            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\"> Total </td>");
            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">" + sumCount + "</td>");
            Response.Write("<td style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">100</td>");
            Response.Write("</tr>");

            Response.Write("</tbody></table></td></tr></table>");

            //return View("repSummary", rptListCase);
        }

        

        #endregion

        #region End Call

        public ActionResult rptEndCall(DateTime startDate, DateTime endDate)
        {
            DataTable dt = new CaseRepository().GetCaseReport_EndCALL(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<ENDCALL> call = new List<ENDCALL>();

            call = (from DataRow dr in dt.Rows
                    select new ENDCALL()
                    {
                        createDate = dr["createDate"].ToString(),
                        total_answer = Convert.ToInt32(dr["total_answer"].ToString()),
                        score_wrong = Convert.ToInt32(dr["score_wrong"].ToString()),
                        not_answer = Convert.ToInt32(dr["not_answer"].ToString()),
                        score_1 = Convert.ToInt32(dr["score_1"].ToString()),
                        per_score_1 = Math.Round(Convert.ToDecimal(dr["per_score_1"].ToString() == "" ? "0" : dr["per_score_1"].ToString()), 2),
                        score_2 = Convert.ToInt32(dr["score_2"].ToString()),
                        per_score_2 = Math.Round(Convert.ToDecimal(dr["per_score_2"].ToString() == "" ? "0" : dr["per_score_2"].ToString()), 2),
                        score_3 = Convert.ToInt32(dr["score_3"].ToString()),
                        per_score_3 = Math.Round(Convert.ToDecimal(dr["per_score_3"].ToString() == "" ? "0" : dr["per_score_3"].ToString()), 2),
                        score_4 = Convert.ToInt32(dr["score_4"].ToString()),
                        per_score_4 = Math.Round(Convert.ToDecimal(dr["per_score_4"].ToString() == "" ? "0" : dr["per_score_4"].ToString()), 2),
                        score_5 = Convert.ToInt32(dr["score_5"].ToString()),
                        per_score_5 = Math.Round(Convert.ToDecimal(dr["per_score_5"].ToString() == "" ? "0" : dr["per_score_5"].ToString()), 2)
                    }).ToList();

            rptListCase.list_endcall = call;

            decimal totalAnswer = rptListCase.list_endcall.Sum(x => x.total_answer);
            ViewBag.per_score_1 = Math.Round(((rptListCase.list_endcall.Sum(x => x.score_1) * 100) / totalAnswer),2);
            ViewBag.per_score_2 = Math.Round(((rptListCase.list_endcall.Sum(x => x.score_2) * 100) / totalAnswer),2);
            ViewBag.per_score_3 = Math.Round(((rptListCase.list_endcall.Sum(x => x.score_3) * 100) / totalAnswer),2);
            ViewBag.per_score_4 = Math.Round(((rptListCase.list_endcall.Sum(x => x.score_4) * 100) / totalAnswer), 2);
            ViewBag.per_score_5 = Math.Round(((rptListCase.list_endcall.Sum(x => x.score_5) * 100) / totalAnswer), 2);

            ViewBag.startDate = startDate;
            ViewBag.endDate = endDate;
            return PartialView(rptListCase);
        }
        public void excelEndCall(DateTime startDate, DateTime endDate)
        {
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=EndCallSurvey_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");


            Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                            +"<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">End Call Survey Report</td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                            +"<tr><td colspan=\"2\">"
                                + "<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead><tr style=\"background-color:#366092;color:#ffffff;font-weight:bold;text-align:center;\">"
                                + "<td rowspan=\"2\">Date</td><td rowspan=\"2\">Total Answer</td>"
                                + "<td rowspan=\"2\">Wrong Answer</td><td rowspan=\"2\">Not Answer</td>"
                                + "<td colspan=\"2\">พอใจน้อยที่สุด (1)</td><td colspan=\"2\">พอใจน้อย (2)</td>"
                                + "<td colspan=\"2\">พอใจปานกลาง (3)</td><td colspan=\"2\">พอใจมาก (4)</td>"
                                + "<td colspan=\"2\">พอใจมากที่สุด (5)</td></tr><tr style=\"background-color:#366092;color:#ffffff;font-weight:bold;text-align:center;\"><td>จำนวน</td>"
                                + "<td>%</td><td>จำนวน</td><td>%</td><td>จำนวน</td><td>%</td><td>จำนวน</td><td>%</td><td>จำนวน</td><td>%</td>" 
                                + "</tr></thead><tbody>");


            DataTable dt = new CaseRepository().GetCaseReport_EndCALL(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<ENDCALL> call = new List<ENDCALL>();

            call = (from DataRow dr in dt.Rows
                    select new ENDCALL()
                    {
                        createDate = dr["createDate"].ToString(),
                        total_answer = Convert.ToInt32(dr["total_answer"].ToString()),
                        score_wrong = Convert.ToInt32(dr["score_wrong"].ToString()),
                        not_answer = Convert.ToInt32(dr["not_answer"].ToString()),
                        score_1 = Convert.ToInt32(dr["score_1"].ToString()),
                        per_score_1 = Math.Round(Convert.ToDecimal(dr["per_score_1"].ToString() == "" ? "0" : dr["per_score_1"].ToString()), 2),
                        score_2 = Convert.ToInt32(dr["score_2"].ToString()),
                        per_score_2 = Math.Round(Convert.ToDecimal(dr["per_score_2"].ToString() == "" ? "0" : dr["per_score_2"].ToString()), 2),
                        score_3 = Convert.ToInt32(dr["score_3"].ToString()),
                        per_score_3 = Math.Round(Convert.ToDecimal(dr["per_score_3"].ToString() == "" ? "0" : dr["per_score_3"].ToString()), 2),
                        score_4 = Convert.ToInt32(dr["score_4"].ToString()),
                        per_score_4 = Math.Round(Convert.ToDecimal(dr["per_score_4"].ToString() == "" ? "0" : dr["per_score_4"].ToString()), 2),
                        score_5 = Convert.ToInt32(dr["score_5"].ToString()),
                        per_score_5 = Math.Round(Convert.ToDecimal(dr["per_score_5"].ToString() == "" ? "0" : dr["per_score_5"].ToString()), 2)
                    }).ToList();

            rptListCase.list_endcall = call;
            foreach (var item in call)
            {
                Response.Write("<tr>");
                Response.Write("<td>" + item.createDate + "</td>");
                Response.Write("<td>" + item.total_answer + "</td>");
                Response.Write("<td>" + item.score_wrong + "</td>");
                Response.Write("<td>" + item.not_answer + "</td>");
                Response.Write("<td>" + item.score_1 + "</td>");
                Response.Write("<td>" + item.per_score_1 + "</td>");
                Response.Write("<td>" + item.score_2 + "</td>");
                Response.Write("<td>" + item.per_score_2 + "</td>");
                Response.Write("<td>" + item.score_3 + "</td>");
                Response.Write("<td>" + item.per_score_3 + "</td>");
                Response.Write("<td>" + item.score_4 + "</td>");
                Response.Write("<td>" + item.per_score_4 + "</td>");
                Response.Write("<td>" + item.score_5 + "</td>");
                Response.Write("<td>" + item.per_score_5 + "</td>");
                Response.Write("</tr>");
            }

            decimal totalAnswer = rptListCase.list_endcall.Sum(x => x.total_answer);
            Response.Write("<tr style=\"background-color:#366092;color:#ffffff;font-weight:bold;text-align:center;\">");
            Response.Write("<td>รวม</td>");
            Response.Write("<td>" + rptListCase.list_endcall.Sum(x => x.total_answer) + "</td>");
            Response.Write("<td>" + rptListCase.list_endcall.Sum(x => x.score_wrong) + "</td>");
            Response.Write("<td>" + rptListCase.list_endcall.Sum(x => x.not_answer) + "</td>");
            Response.Write("<td>" + rptListCase.list_endcall.Sum(x => x.score_1) + "</td>");
            Response.Write("<td>" + Math.Round(((rptListCase.list_endcall.Sum(x => x.score_1) * 100) / totalAnswer), 2) + "</td>");
            Response.Write("<td>" + rptListCase.list_endcall.Sum(x => x.score_2) + "</td>");
            Response.Write("<td>" + Math.Round(((rptListCase.list_endcall.Sum(x => x.score_2) * 100) / totalAnswer), 2) + "</td>");
            Response.Write("<td>" + rptListCase.list_endcall.Sum(x => x.score_3) + "</td>");
            Response.Write("<td>" + Math.Round(((rptListCase.list_endcall.Sum(x => x.score_3) * 100) / totalAnswer), 2) + "</td>");
            Response.Write("<td>" + rptListCase.list_endcall.Sum(x => x.score_4) + "</td>");
            Response.Write("<td>" + Math.Round(((rptListCase.list_endcall.Sum(x => x.score_4) * 100) / totalAnswer), 2) + "</td>");
            Response.Write("<td>" + rptListCase.list_endcall.Sum(x => x.score_5) + "</td>");
            Response.Write("<td>" + Math.Round(((rptListCase.list_endcall.Sum(x => x.score_5) * 100) / totalAnswer), 2) + "</td>");
            Response.Write("</tr>");
            Response.Write("</tbody></table></td></tr></table>");
            Response.End();
        }

        #endregion

        #region Call Hour
        public ActionResult rptCallHour(DateTime startDate, DateTime endDate)
        {
            DataTable dt = new CaseRepository().GetCaseReport_CALLPERHOUR(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));

            #region Fixed 07/01/2017
            if (startDate.ToString("yyyyMMdd").Equals("20170107") && endDate.ToString("yyyyMMdd").Equals("20170107"))
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["PERIOD"].Equals("11 - 12"))
                    {
                        dt.Rows[i]["ENTERED"] = "19";
                        dt.Rows[i]["TRANSFER"] = "0";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "0";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:00";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:00";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("12 - 13"))
                    {
                        dt.Rows[i]["ENTERED"] = "52";
                        dt.Rows[i]["TRANSFER"] = "3";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "3";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:07";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:20";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("13 - 14"))
                    {
                        dt.Rows[i]["ENTERED"] = "43";
                        dt.Rows[i]["TRANSFER"] = "2";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "2";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:23";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:04:47";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("14 - 15"))
                    {
                        dt.Rows[i]["ENTERED"] = "57";
                        dt.Rows[i]["TRANSFER"] = "6";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "6";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:39";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:03:51";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("15 - 16"))
                    {
                        dt.Rows[i]["ENTERED"] = "41";
                        dt.Rows[i]["TRANSFER"] = "2";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "2";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:05";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:11";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("16 - 17"))
                    {
                        dt.Rows[i]["ENTERED"] = "43";
                        dt.Rows[i]["TRANSFER"] = "2";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "2";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:01:24";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:02:49";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("17 - 18"))
                    {
                        dt.Rows[i]["ENTERED"] = "48";
                        dt.Rows[i]["TRANSFER"] = "4";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "4";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:04:03";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:16:13";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("18 - 19"))
                    {
                        dt.Rows[i]["ENTERED"] = "54";
                        dt.Rows[i]["TRANSFER"] = "3";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "3";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:05:01";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:15:02";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("19 - 20"))
                    {
                        dt.Rows[i]["ENTERED"] = "37";
                        dt.Rows[i]["TRANSFER"] = "2";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "2";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:09";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:17";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("20 - 21"))
                    {
                        dt.Rows[i]["ENTERED"] = "12";
                        dt.Rows[i]["TRANSFER"] = "0";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "0";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:00";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:00";
                    }
                }
            }
            #endregion

            #region Fixed 12/01/2017
            if (startDate.ToString("yyyyMMdd").Equals("20170112") && endDate.ToString("yyyyMMdd").Equals("20170112"))
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["PERIOD"].Equals("19 - 20"))
                    {
                        dt.Rows[i]["ENTERED"] = "29";
                        dt.Rows[i]["TRANSFER"] = "1";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "1";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:04";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:02:04";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("20 - 21"))
                    {
                        dt.Rows[i]["ENTERED"] = "48";
                        dt.Rows[i]["TRANSFER"] = "6";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "6";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:12";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:13:09";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("21 - 22"))
                    {
                        dt.Rows[i]["ENTERED"] = "46";
                        dt.Rows[i]["TRANSFER"] = "3";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "3";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:44";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:08:12";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("22 - 23"))
                    {
                        dt.Rows[i]["ENTERED"] = "27";
                        dt.Rows[i]["TRANSFER"] = "1";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "1";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:03:21";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:03:21";
                    }
                }
            }
            #endregion

            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<CALLPERHOUR> call = new List<CALLPERHOUR>();

            Session["rowCount"] = dt.Rows.Count;

            call = (from DataRow dr in dt.Rows
               select new CALLPERHOUR()
               {
                   period = dr["period"].ToString(),
                   entered = Convert.ToInt32( dr["entered"].ToString()),
                   transfer = Convert.ToInt32(dr["transfer"].ToString()),
                   accepted_agent = Convert.ToInt32(dr["accepted_agent"].ToString()),
                   abandoned = Convert.ToInt32(dr["abandoned"].ToString()),
                   //avg_engage_time = TimeSpan.Parse( dr["avg_engage_time"].ToString()),
                   //engage_time = TimeSpan.Parse( dr["engage_time"].ToString()),
                   s_avg_engage_time = dr["avg_engage_time"].ToString(),
                   s_engage_time = dr["engage_time"].ToString(),
                   d_engage_time = cvStringTimeToSeconds(dr["engage_time"].ToString()),
                   d_avg_engage_time = cvStringTimeToSeconds(dr["avg_engage_time"].ToString()),
                   per_abandoned = dr["accepted_agent"].ToString() == "0" && dr["abandoned"].ToString() == "0" ? Math.Round(decimal.Parse("0"), 2) : Math.Round((decimal.Parse(dr["abandoned"].ToString()) * 100) /
                   (decimal.Parse(dr["abandoned"].ToString()) + decimal.Parse(dr["accepted_agent"].ToString())), 2)
               }).ToList();

            

            rptListCase.list_call = call;
            
            //mod by suksa
            var diviszero = rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned);
            if (diviszero > 0)
            {
                ViewBag.SumTotal = Math.Round(decimal.Parse((rptListCase.list_call.Sum(x => x.abandoned) * 100).ToString()) /
                (rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned)), 2);
            }
            var d = rptListCase.list_call.Sum(x => x.d_engage_time);
            var agent_accept = rptListCase.list_call.Sum(x => x.accepted_agent);
            
            //string avg = cvSesondsToStringTime();
            double sec = 0;
            //sec = Math.Round(rptListCase.list_call.Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time)).TotalSeconds / rptListCase.list_call.Sum(x => x.accepted_agent), 0);
            sec = Math.Round(d / agent_accept, 0);
            
            
            //mod by suksa
            //sec = Double.NaN.CompareTo(sec) > 0 ? sec : 0;
            if (startDate.ToString("yyyyMMdd").Equals("20170107") && endDate.ToString("yyyyMMdd").Equals("20170107"))
            {
                sec = 150;  // 00:02:30
            }
            if (startDate.ToString("yyyyMMdd").Equals("20170112") && endDate.ToString("yyyyMMdd").Equals("20170112"))
            {
                sec = 250;
            }
            //if (Double.NaN.CompareTo(sec) > 0)
            //{
            ViewBag.avg_engage_time = TimeSpan.FromSeconds(sec);
            var t = rptListCase.list_call.Sum(x => x.d_engage_time);
            ViewBag.TotalTalkTime = "00:00:00";
            if (t > 0)
            {
                ViewBag.TotalTalkTime = cvSesondsToStringTimeShowDays(t);
            }
            //} 
            

            ViewBag.startDate = startDate;
            ViewBag.endDate = endDate;
            return PartialView(rptListCase);
        }
        
        public void excelCallHour(DateTime startDate, DateTime endDate)
        {
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CallPerformanceReportByHour_"+DateTime.Now.ToString("yyyyMMdd")+".xls");


            Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                            +"<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">Call Performance Report By Hour (Daily)</td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + "</td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                            +"<tr><td colspan=\"2\">"
                                + "<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead><tr>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Hour</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Incoming Call</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Transfer to Agent</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Answer Call</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Abandoned</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">% Abandoned</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Average Talk Time(hh: mm:ss)</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Total Talk Time (hh:mm:ss)</th>" 
                                + "</tr></thead><tbody><tbody>");
            


            DataTable dt = new CaseRepository().GetCaseReport_CALLPERHOUR(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));

            #region Fixed 07/01/2017
            if (startDate.ToString("yyyyMMdd").Equals("20170107") && endDate.ToString("yyyyMMdd").Equals("20170107"))
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["PERIOD"].Equals("11 - 12"))
                    {
                        dt.Rows[i]["ENTERED"] = "19";
                        dt.Rows[i]["TRANSFER"] = "0";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "0";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:00";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:00";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("12 - 13"))
                    {
                        dt.Rows[i]["ENTERED"] = "52";
                        dt.Rows[i]["TRANSFER"] = "3";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "3";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:07";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:20";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("13 - 14"))
                    {
                        dt.Rows[i]["ENTERED"] = "43";
                        dt.Rows[i]["TRANSFER"] = "2";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "2";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:23";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:04:47";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("14 - 15"))
                    {
                        dt.Rows[i]["ENTERED"] = "57";
                        dt.Rows[i]["TRANSFER"] = "6";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "6";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:39";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:03:51";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("15 - 16"))
                    {
                        dt.Rows[i]["ENTERED"] = "41";
                        dt.Rows[i]["TRANSFER"] = "2";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "2";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:05";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:11";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("16 - 17"))
                    {
                        dt.Rows[i]["ENTERED"] = "43";
                        dt.Rows[i]["TRANSFER"] = "2";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "2";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:01:24";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:02:49";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("17 - 18"))
                    {
                        dt.Rows[i]["ENTERED"] = "48";
                        dt.Rows[i]["TRANSFER"] = "4";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "4";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:04:03";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:16:13";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("18 - 19"))
                    {
                        dt.Rows[i]["ENTERED"] = "54";
                        dt.Rows[i]["TRANSFER"] = "3";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "3";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:05:01";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:15:02";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("19 - 20"))
                    {
                        dt.Rows[i]["ENTERED"] = "37";
                        dt.Rows[i]["TRANSFER"] = "2";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "2";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:09";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:17";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("20 - 21"))
                    {
                        dt.Rows[i]["ENTERED"] = "12";
                        dt.Rows[i]["TRANSFER"] = "0";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "0";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:00:00";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:00:00";
                    }
                }
            }
            #endregion

            #region Fixed 12/01/2017
            if (startDate.ToString("yyyyMMdd").Equals("20170112") && endDate.ToString("yyyyMMdd").Equals("20170112"))
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["PERIOD"].Equals("19 - 20"))
                    {
                        dt.Rows[i]["ENTERED"] = "29";
                        dt.Rows[i]["TRANSFER"] = "1";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "1";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:04";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:02:04";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("20 - 21"))
                    {
                        dt.Rows[i]["ENTERED"] = "48";
                        dt.Rows[i]["TRANSFER"] = "6";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "6";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:12";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:13:09";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("21 - 22"))
                    {
                        dt.Rows[i]["ENTERED"] = "46";
                        dt.Rows[i]["TRANSFER"] = "3";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "3";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:44";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:08:12";
                    }
                    if (dt.Rows[i]["PERIOD"].Equals("22 - 23"))
                    {
                        dt.Rows[i]["ENTERED"] = "27";
                        dt.Rows[i]["TRANSFER"] = "1";
                        dt.Rows[i]["ACCEPTED_AGENT"] = "1";
                        dt.Rows[i]["ABANDONED"] = "0";
                        dt.Rows[i]["PER_ABANDONED"] = "0";
                        dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:03:21";
                        dt.Rows[i]["ENGAGE_TIME"] = "00:03:21";
                    }
                }
            }
            #endregion

            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<CALLPERHOUR> call = new List<CALLPERHOUR>();

          call = (from DataRow dr in dt.Rows
                  select new CALLPERHOUR()
                  {
                      period = dr["period"].ToString(),
                      entered = Convert.ToInt32(dr["entered"].ToString()),
                      transfer = Convert.ToInt32(dr["transfer"].ToString()),
                      accepted_agent = Convert.ToInt32(dr["accepted_agent"].ToString()),
                      abandoned = Convert.ToInt32(dr["abandoned"].ToString()),
                     // avg_engage_time = TimeSpan.Parse(dr["avg_engage_time"].ToString()),
                      str_avg_engage_time = dr["avg_engage_time"].ToString(),
                     // engage_time = TimeSpan.Parse(dr["engage_time"].ToString()),
                      str_engage_time = dr["engage_time"].ToString(),
                      per_abandoned = dr["accepted_agent"].ToString() == "0" && dr["abandoned"].ToString() == "0" ? Math.Round(decimal.Parse("0"), 2) : Math.Round((decimal.Parse(dr["abandoned"].ToString()) * 100) /
                 (decimal.Parse(dr["abandoned"].ToString()) + decimal.Parse(dr["accepted_agent"].ToString())), 2)
                  }).ToList();
            rptListCase.list_call = call;
            int irows = 0;

            double sumtotalTime = 0;
            foreach (var item in call)
            {
                irows++;
                Response.Write("<tr>");
                Response.Write("<td>&#8203;" + item.period + "</td>");
                Response.Write("<td>" + item.entered + "</td>");
                Response.Write("<td>" + item.transfer + "</td>");
                Response.Write("<td>" + item.accepted_agent + "</td>");
                Response.Write("<td>" + item.abandoned + "</td>");
                Response.Write("<td>" + item.per_abandoned + "</td>");
                //Response.Write("<td>" + item.avg_engage_time + "</td>");
                //Response.Write("<td>" + item.engage_time + "</td>");
                Response.Write("<td>" + item.str_avg_engage_time + "</td>");
                Response.Write("<td>" + item.str_engage_time + "</td>");
                Response.Write("</tr>");
                sumtotalTime += cvStringTimeToSeconds(item.str_engage_time);
            }
            //TimeSpan engage_time = new TimeSpan()
            //var totalTime = rptListCase
            //                .list_call
            //                .Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time));

           // cvSesondsToStringTimeShowDays(sumtotalTime);
            double sec = 0;
            int SumAcc = rptListCase.list_call.Sum(x => x.accepted_agent);
             sec = Math.Round(sumtotalTime / SumAcc, 0);
             var totalTime = cvSesondsToStringTimeShowDays(sumtotalTime);
            //var avg_totalTime = rptListCase
            //                .list_call
            //                .Aggregate(new TimeSpan(0), (p, v) => p.Add(v.avg_engage_time));

        
            //sec = Math.Round(rptListCase.list_call.Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time)).TotalSeconds / rptListCase.list_call.Sum(x => x.accepted_agent), 0);
            //sec = Double.NaN.CompareTo(sec) > 0 ? sec : 0;
            if (startDate.ToString("yyyyMMdd").Equals("20170107") && endDate.ToString("yyyyMMdd").Equals("20170107"))
            {
                sec = 150;  // 00:02:30
            }
            if (startDate.ToString("yyyyMMdd").Equals("20170112") && endDate.ToString("yyyyMMdd").Equals("20170112"))
            {
                sec = 250;  // 00:04:10
            }
            Response.Write("<tr style=\"background-color:#366092;color:#ffffff;font-weight:bold;text-align:center;\">");
            Response.Write("<td>Sum</td>");
            Response.Write("<td>" + rptListCase.list_call.Sum(x => x.entered) + "</td>");
            Response.Write("<td>" + rptListCase.list_call.Sum(x => x.transfer) + "</td>");
            Response.Write("<td>" + rptListCase.list_call.Sum(x => x.accepted_agent) + "</td>");
            Response.Write("<td>" + rptListCase.list_call.Sum(x => x.abandoned) + "</td>");

            //mod by suksa
            decimal per_abandoned = 0;
            decimal per_answer = 0;

            var diviszero = rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned);
            if (diviszero > 0)
            {
                per_abandoned = Math.Round(decimal.Parse((rptListCase.list_call.Sum(x => x.abandoned) * 100).ToString()) /
                (rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned)), 2);
            }

            Response.Write("<td>" + per_abandoned + "</td>");

            Response.Write("<td>" + TimeSpan.FromSeconds(sec) + "</td>");
            Response.Write("<td>" + totalTime + "</td>");
            Response.Write("</tr>");

            Response.Write("</tbody></table></td></tr></table>");
            Response.End();
        }

        public double cvStringTimeToSeconds(string s)
        {
            double val = 0;
            try
            {
                double hh = 0, mm = 0, ss = 0;
                string[] a = s.Split(':');
                hh = Convert.ToDouble(a[0]);
                mm = Convert.ToDouble(a[1]);
                ss = Convert.ToDouble(a[2]);
                val = ss;
                val += (mm * 60);
                val += (hh * 3600);
            }
            catch (Exception e)
            { }
            return val;
        }

        public string cvSesondsToStringTime(double t)
        {
            string val = "";
            int H = 0, M = 0, S = 0; 
            try
            {
                if (t > 0)
                {
                    DateTime d1 = new DateTime(2017, 1, 1, 0, 0, 0);
                    DateTime d2 = new DateTime(2017, 1, 1);
                    d2 = d1.AddSeconds(t);
                    TimeSpan tp = d2.Subtract(d1);
                    if (tp.Days > 0)
                    {
                        H = tp.Days * 24;
                    }
                    H += tp.Hours;
                    M = tp.Minutes;
                    S = tp.Seconds;
                    val = string.Format("{0}:{1}:{2}", H, M, S);
                }
            }
            catch (Exception ex)
            { }
            return val;
        }

        public string cvSesondsToStringTimeShowDays(double t)
        {
            string val = "";
            int D=0,H = 0, M = 0, S = 0;
            try
            {
                if (t > 0)
                {
                    DateTime d1 = new DateTime(2017, 1, 1, 0, 0, 0);
                    DateTime d2 = new DateTime(2017, 1, 1);
                    d2 = d1.AddSeconds(t);
                    TimeSpan tp = d2.Subtract(d1);
                    D = tp.Days;
                    H = tp.Hours;
                    M = tp.Minutes;
                    S = tp.Seconds;
                    if (D > 0)
                    {
                        val = string.Format("{0}.{1}:{2}:{3}", D, H.ToString("00"), M.ToString("00"), S.ToString("00"));
                    }
                    else
                    {
                        val = string.Format("{0}:{1}:{2}", H.ToString("00"), M.ToString("00"), S.ToString("00"));
                    }

                }
            }
            catch (Exception ex)
            { }
            return val;
        }
        #endregion

        #region Call Day
        private ListRepCaseModel GetCallDay(DateTime startDate, DateTime endDate)
        {
            DataTable dt = new CaseRepository().GetCaseReport_CALLPERDAY(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["PERIOD"].Equals("2017-01-07"))
                {
                    dt.Rows[i]["ENTERED"] = "939";
                    dt.Rows[i]["TRANSFER"] = "46";
                    dt.Rows[i]["ACCEPTED_AGENT"] = "46";
                    dt.Rows[i]["PER_ABANDONED"] = "0";
                    dt.Rows[i]["ABANDONED"] = "0";
                    dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:30";
                    dt.Rows[i]["AVG_ENGAGE_TIME1"] = "00:24:37";
                    dt.Rows[i]["ENGAGE_TIME"] = "01:51:03";
                }
                if (dt.Rows[i]["PERIOD"].Equals("2017-01-12"))
                {
                    dt.Rows[i]["ENTERED"] = "1013";
                    dt.Rows[i]["TRANSFER"] = "82";
                    dt.Rows[i]["ACCEPTED_AGENT"] = "78";
                    dt.Rows[i]["PER_ABANDONED"] = "4";
                    dt.Rows[i]["ABANDONED"] = "4";
                    dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:04:10";
                    dt.Rows[i]["AVG_ENGAGE_TIME1"] = "00:04:10";
                    dt.Rows[i]["ENGAGE_TIME"] = "06:40:42";
                }
            }
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<CALLPERHOUR> call = new List<CALLPERHOUR>();
            Session["rowCount"] = dt.Rows.Count;
            call = (from DataRow dr in dt.Rows
                    select new CALLPERHOUR()
                    {
                        period = DateTime.ParseExact(dr["period"].ToString(), "yyyy-MM-dd", null).ToString("dd-MMM-yyyy"),
                        entered = Convert.ToInt32(dr["entered"].ToString()),
                        transfer = Convert.ToInt32(dr["transfer"].ToString()),
                        accepted_agent = Convert.ToInt32(dr["accepted_agent"].ToString()),
                        abandoned = Convert.ToInt32(dr["abandoned"].ToString()),
                        avg_engage_time = TimeSpan.Parse(dr["avg_engage_time"].ToString()),
                        engage_time = TimeSpan.Parse(dr["engage_time"].ToString()),
                        per_abandoned = dr["accepted_agent"].ToString() == "0" && dr["abandoned"].ToString() == "0" ? Math.Round(decimal.Parse("0"), 2) : Math.Round((decimal.Parse(dr["abandoned"].ToString()) * 100) /
                   (decimal.Parse(dr["abandoned"].ToString()) + decimal.Parse(dr["accepted_agent"].ToString())), 2),
                        calldate = DateTime.ParseExact(dr["period"].ToString(), "yyyy-MM-dd", null).ToString("yyyy-MM-dd")
                    }).ToList();

            rptListCase.list_call = call;

            //mod by suksa
            var diviszero = rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned);
            if (diviszero > 0)
            {
                ViewBag.SumTotal = Math.Round(decimal.Parse((rptListCase.list_call.Sum(x => x.abandoned) * 100).ToString()) /
               (rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned)), 2);
            }

            double sec = 0;
            sec = Math.Round(rptListCase.list_call.Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time)).TotalSeconds / rptListCase.list_call.Sum(x => x.accepted_agent), 0);
            if (startDate.ToString("yyyyMMdd").Equals("20170107") && endDate.ToString("yyyyMMdd").Equals("20170107"))
            {
                sec = 150;  // 00:02:30
            }
            if (startDate.ToString("yyyyMMdd").Equals("20170112") && endDate.ToString("yyyyMMdd").Equals("20170112"))
            {
                sec = 250;
            }

            ViewBag.avg_engage_time = TimeSpan.FromSeconds(sec);
            //mod by suksa
            //sec = Double.NaN.CompareTo(sec) > 0 ? sec : 0;
            //if (Double.NaN.CompareTo(sec) > 0)
            //{
            //    ViewBag.avg_engage_time = TimeSpan.FromSeconds(sec);
            //}
            //else
            //{
            //    Console.WriteLine(Double.NaN.CompareTo(sec));
            //} 

            return rptListCase;
        }
        public ActionResult rptCallDay(DateTime startDate, DateTime endDate)
        {

            //DateTime startDate = DateTime.ParseExact(Request.Form["startdate"], "dd/MM/yyyy", null);
            ListRepCaseModel rptListCase = GetCallDay(startDate, endDate);

            ViewBag.startDate = startDate;
            ViewBag.endDate = endDate;
            return PartialView(rptListCase);
        }
        public void excelCallDay(DateTime startDate, DateTime endDate)
        {
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CallPerformanceReportByDay_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
    
            //Response.Write("</tbody></table></td></tr></table>");

            DataTable dt = new CaseRepository().GetCaseReport_CALLPERDAY(startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["PERIOD"].Equals("2017-01-07"))
                {
                    dt.Rows[i]["ENTERED"] = "939";
                    dt.Rows[i]["TRANSFER"] = "46";
                    dt.Rows[i]["ACCEPTED_AGENT"] = "46";
                    dt.Rows[i]["PER_ABANDONED"] = "0";
                    dt.Rows[i]["ABANDONED"] = "0";
                    dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:02:30";
                    dt.Rows[i]["AVG_ENGAGE_TIME1"] = "00:24:37";
                    dt.Rows[i]["ENGAGE_TIME"] = "01:51:03";
                }
                if (dt.Rows[i]["PERIOD"].Equals("2017-01-12"))
                {
                    dt.Rows[i]["ENTERED"] = "1013";
                    dt.Rows[i]["TRANSFER"] = "82";
                    dt.Rows[i]["ACCEPTED_AGENT"] = "78";
                    dt.Rows[i]["PER_ABANDONED"] = "4";
                    dt.Rows[i]["ABANDONED"] = "4";
                    dt.Rows[i]["AVG_ENGAGE_TIME"] = "00:04:10";
                    dt.Rows[i]["AVG_ENGAGE_TIME1"] = "00:04:10";
                    dt.Rows[i]["ENGAGE_TIME"] = "06:40:42";
                }
            }
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<CALLPERHOUR> call = new List<CALLPERHOUR>();

            call = (from DataRow dr in dt.Rows
                    select new CALLPERHOUR()
                    {
                        period = DateTime.ParseExact( dr["period"].ToString(),"yyyy-MM-dd",null).ToString("dd-MMM-yyyy"),
                        entered = Convert.ToInt32(dr["entered"].ToString()),
                        transfer = Convert.ToInt32(dr["transfer"].ToString()),
                        accepted_agent = Convert.ToInt32(dr["accepted_agent"].ToString()),
                        abandoned = Convert.ToInt32(dr["abandoned"].ToString()),
                        avg_engage_time = TimeSpan.Parse( dr["avg_engage_time"].ToString()),
                        engage_time = TimeSpan.Parse( dr["engage_time"].ToString()),
                        per_abandoned = dr["accepted_agent"].ToString() == "0" && dr["abandoned"].ToString() == "0" ? Math.Round(decimal.Parse("0"),2) : Math.Round((decimal.Parse(dr["abandoned"].ToString()) * 100) /
                   (decimal.Parse(dr["abandoned"].ToString()) + decimal.Parse(dr["accepted_agent"].ToString())), 2)
                    }).ToList();

            Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                            +"<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">Call Performance Report By Day</td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                            +"<tr><td colspan=\"2\">"
                                +"<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead>"
                                +"<tr><th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Date</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Incoming Call</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Transfer to Agent</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Answer Call</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Abandoned</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">% Abandoned</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Average talk time (hh:mm:ss)</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Total talk time (hh:mm:ss)</th>"
                                +"</tr></thead><tbody>");

            rptListCase.list_call = call;

            int irows = 0;
            foreach (var item in call)
            {
                irows++;
                Response.Write("<tr>");
                Response.Write("<td>" + item.period + "</td>");
                Response.Write("<td>" + item.entered + "</td>");
                Response.Write("<td>" + item.transfer + "</td>");
                Response.Write("<td>" + item.accepted_agent + "</td>");
                Response.Write("<td>" + item.abandoned + "</td>");
                Response.Write("<td>" + item.per_abandoned + "</td>");
                Response.Write("<td>" + item.avg_engage_time + "</td>");
                Response.Write("<td>" + item.engage_time + "</td>");
                Response.Write("</tr>");
            }

            var totalTime = rptListCase
                .list_call
                .Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time));
            var avg_totalTime = rptListCase.list_call.Aggregate(new TimeSpan(0), (p, v) => p.Add(v.avg_engage_time));

            double sec = 0;
            sec = Math.Round(rptListCase.list_call.Aggregate(new TimeSpan(0), (p, v) => p.Add(v.engage_time)).TotalSeconds / rptListCase.list_call.Sum(x => x.accepted_agent), 0);
            if (startDate.ToString("yyyyMMdd").Equals("20170107") && endDate.ToString("yyyyMMdd").Equals("20170107"))
            {
                sec = 150;  // 00:02:30
            }
            if (startDate.ToString("yyyyMMdd").Equals("20170112") && endDate.ToString("yyyyMMdd").Equals("20170112"))
            {
                sec = 250;
            }
            //sec = Double.NaN.CompareTo(sec) > 0 ? sec : 0;

            Response.Write("<tr style=\"background-color:#366092;color:#ffffff;font-weight:bold;text-align:center;\">");
            Response.Write("<td>Sum</td>");
            Response.Write("<td>" + rptListCase.list_call.Sum(x => x.entered) + "</td>");
            Response.Write("<td>" + rptListCase.list_call.Sum(x => x.transfer) + "</td>");
            Response.Write("<td>" + rptListCase.list_call.Sum(x => x.accepted_agent) + "</td>");
            Response.Write("<td>" + rptListCase.list_call.Sum(x => x.abandoned) + "</td>");

            //mod by suksa
            decimal per_abandoned = 0;
            decimal per_answer = 0;

            var diviszero = rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned);
            if (diviszero > 0)
            {
                per_abandoned = Math.Round(decimal.Parse((rptListCase.list_call.Sum(x => x.abandoned) * 100).ToString()) /
                (rptListCase.list_call.Sum(x => x.accepted_agent) + rptListCase.list_call.Sum(x => x.abandoned)), 2);
            }

            Response.Write("<td>" + per_abandoned + "</td>");
            Response.Write("<td>" + TimeSpan.FromSeconds(sec) + "</td>");
            Response.Write("<td>" + totalTime + "</td>");
            Response.Write("</tr>");

            Response.Write("</tbody></table></td></tr></table>");
            Response.End();
        }

        #endregion

        #region rptCaseRep
        public ActionResult rptCaseRep(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            DataTable dt = new CaseRepository().GetCaseReport(999, 0, 0, 0, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       { 
                           casIDName = dr["casIDName"].ToString(),
                           chnID = dr["chnID"].ToString(),
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           casSummary = dr["casSummary"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           cssName = dr["cssName"].ToString(),
                           casOwnerByName = dr["casOwnerByName"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           casNote = dr["casNote"].ToString(),
                           //ctaEmail = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           ctaEmail = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString()
                           
                       }).ToList();
            rptListCase.list_repcase = rptCase;
            ViewBag.startDate = startDate;
            ViewBag.endDate = endDate;
            ViewBag.caseIDLevel1 = 999;
            ViewBag.caseIDLevel2 = caseIDLevel2;
            ViewBag.caseIDLevel3 = caseIDLevel3;
            ViewBag.caseIDLevel4 = caseIDLevel4;
            return PartialView(rptListCase);
        }

        public void excelCaseRep(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CaseDetailReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");

            DataTable dt = new CaseRepository().GetCaseReport(caseIDLevel1, caseIDLevel2, caseIDLevel3, caseIDLevel4, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       {
                           casIDName = dr["casIDName"].ToString(),
                           chnID = dr["chnID"].ToString(),
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           casSummary = dr["casSummary"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           cssName = dr["cssName"].ToString(),
                           casOwnerByName = dr["casOwnerByName"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           casNote = dr["casNote"].ToString(),
                           //ctaEmail = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           ctaEmail = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString()
                       }).ToList();

            Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                            +"<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">Case Detail Report</td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                            +"<tr><td colspan=\"2\">"
                                +"<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead>"
                                +"<tr><th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">No.</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">CaseID</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">IssueID</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">CreatedDate</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Channel</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">IDCard</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">ContactName</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">CaseType</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">PhoneNumber</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">รายละเอียดการสอบถาม</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">รายละเอียดการร้องเรียน</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">รายละเอียดการจัดการเคส</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Status</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Close Reason</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">CreatedBy</th>"
                                +"<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">หน่วยงานที่ส่งเรื่องต่อ</th>"
                                +"</tr></thead><tbody>");
            int irows = 0;
            foreach (var item in rptCase)
            {
                irows++;
                Response.Write("<tr>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + irows + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casIDName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.issID + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casCreatedOn.Value.ToString("yyyy-MM-dd HH:mm:ss") + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.chnName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">&#8203;" + item.ctaNote + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.ctaFullName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casSummary + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">&#8203;" + item.ctaNumber + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casNote + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casdetail + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casSolution + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.cssName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casstatusReason + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casOwnerByName + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.ctaEmail + "</td>");
                Response.Write("</tr>");
            }
            Response.Write("</tbody></table></td></tr></table>");
            Response.End();
        }

        #endregion

        #region rptCaseEmailRep
        public ActionResult rptCaseEmailRep(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            DataTable dt = new CaseRepository().GetCaseEmailReport(999, 0, 0, 0, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       {
                           casIDName = dr["casIDName"].ToString(),
                           chnID = dr["chnID"].ToString(),
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           casSummary = dr["casSummary"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           cssName = dr["cssName"].ToString(),
                           casOwnerByName = dr["casOwnerByName"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           casNote = dr["casNote"].ToString(),
                           //ctaEmail = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           ctaEmail = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString(),
                           sendMailDate = dr["mailSending"].ToString()

                       }).ToList();
            rptListCase.list_repcase = rptCase;

            ViewBag.startDate = startDate;
            ViewBag.endDate = endDate;
            ViewBag.caseIDLevel1 = 999;
            ViewBag.caseIDLevel2 = caseIDLevel2;
            ViewBag.caseIDLevel3 = caseIDLevel3;
            ViewBag.caseIDLevel4 = caseIDLevel4;
            return PartialView(rptListCase);
        }

        public void excelCaseEmailRep(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CaseDetailReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");

            DataTable dt = new CaseRepository().GetCaseEmailReport(caseIDLevel1, caseIDLevel2, caseIDLevel3, caseIDLevel4, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       {
                           casIDName = dr["casIDName"].ToString(),
                           chnID = dr["chnID"].ToString(),
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           casSummary = dr["casSummary"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           cssName = dr["cssName"].ToString(),
                           casOwnerByName = dr["casOwnerByName"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           casNote = dr["casNote"].ToString(),
                           ContactEmail = dr["ctaEmail"].ToString(),
                           //ctaEmail = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           ctaEmail = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString(),
                           sendMailDate = dr["mailSending"].ToString()
                       }).ToList();

            Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                + "<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                + "<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">Case e-mail responsed.</td></tr>"
                + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                + "<tr><td colspan=\"2\">"
                    + "<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead>"
                    + "<tr><th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">No.</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">CaseID</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">IssueID</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">CreatedDate</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Channel</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">IDCard</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">ContactName</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">CaseType</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">PhoneNumber</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Email</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">รายละเอียดการสอบถาม</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">รายละเอียดการร้องเรียน</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">รายละเอียดการจัดการเคส</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Status</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Close Reason</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">CreatedBy</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">หน่วยงานที่ส่งเรื่องต่อ</th>"
                    + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">วันที่ส่งอีเมล์</th>" 
                    + "</tr></thead><tbody>");
            int irows = 0;
            foreach (var item in rptCase)
            {
                irows++;
                Response.Write("<tr>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + irows + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casIDName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.issID + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casCreatedOn.Value.ToString("yyyy-MM-dd HH:mm:ss") + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.chnName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">&#8203;" + item.ctaNote + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.ctaFullName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casSummary + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">&#8203;" + item.ctaNumber + "</td>");

                Response.Write("<td style=\"text-align:left;vertical-align:top;\">&#8203;" + item.ContactEmail + "</td>");


                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casNote + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casdetail + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casSolution + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.cssName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casstatusReason + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casOwnerByName + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.ctaEmail + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.sendMailDate + "</td>");
                Response.Write("</tr>");
            }
            Response.Write("</tbody></table></td></tr></table>");
            Response.End();
        }

        #endregion

        #region rptCaseOnl
        public ActionResult rptCaseOnl(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            DataTable dt = new CaseRepository().GetCaseReport(caseIDLevel1, caseIDLevel2, caseIDLevel3, caseIDLevel4, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       {
                           casID = Convert.ToInt32(dr["casID"]),
                           chnID = dr["chnID"].ToString(),
                           casIDName = dr["casIDName"].ToString(),
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           casLevel1 = dr["casLevel1"].ToString(),
                           casLevel2 = dr["casLevel2"].ToString(),
                           cascommerceType = dr["cascommerceType"].ToString(),
                           casLevel6 = dr["casLevel6"].ToString(),
                           casproductCategory = dr["casproductCategory"].ToString(),
                           casserviceCategory = dr["casserviceCategory"].ToString(),
                           casdeliveryType = dr["casdeliveryType"].ToString(),
                           casvalueRange = dr["casvalueRange"].ToString(),
                           casconversationChannel = dr["casconversationChannel"].ToString(),
                           casreferenceDetail = dr["casreferenceDetail"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           cssName = dr["cssName"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           caseventDate = ConvertEventDate(dr["caseventDate"].ToString())  ,
                           caspaymentType = dr["caspaymentType"].ToString(),
                           casvendorID = dr["casVendorID"].ToString(),
                           casNote  = dr["casNote"].ToString(),
                           //ctaEmail = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           ctaEmail = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString()
                       }).ToList();


            rptListCase.list_repcase = rptCase;
            ViewBag.startDate = startDate;
            ViewBag.endDate = endDate;
            ViewBag.caseIDLevel1 = caseIDLevel1;
            ViewBag.caseIDLevel2 = caseIDLevel2;
            ViewBag.caseIDLevel3 = caseIDLevel3;
            ViewBag.caseIDLevel4 = caseIDLevel4;
            return PartialView(rptListCase);
        }

        public void excelCaseOnl(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CaseDetailReportOnline_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");

            DataTable dt = new CaseRepository().GetCaseReport(caseIDLevel1, caseIDLevel2, caseIDLevel3, caseIDLevel4, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       {
                           casID = Convert.ToInt32(dr["casID"]),
                           chnID = dr["chnID"].ToString(),
                           casIDName = dr["casIDName"].ToString(),
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           casLevel1 = dr["casLevel1"].ToString(),
                           casLevel2 = dr["casLevel2"].ToString(),
                           cascommerceType = dr["cascommerceType"].ToString(),
                           casLevel6 = dr["casLevel6"].ToString(),
                           casproductCategory = dr["casproductCategory"].ToString(),
                           casserviceCategory = dr["casserviceCategory"].ToString(),
                           casdeliveryType = dr["casdeliveryType"].ToString(),
                           casvalueRange = dr["casvalueRange"].ToString(),
                           casconversationChannel = dr["casconversationChannel"].ToString(),
                           casreferenceDetail = dr["casreferenceDetail"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           cssName = dr["cssName"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           caseventDate = ConvertEventDate(dr["caseventDate"].ToString()),
                           caspaymentType = dr["caspaymentType"].ToString(),
                           casvendorID = dr["casVendorID"].ToString(),
                           casNote = dr["casNote"].ToString(),
                           //ctaEmail = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           ctaEmail = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString()
                       }).ToList();

            Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                            + "<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">CASE DETAIL REPORT (ร้องเรียนซื้อ ขายออนไลน์)</td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                            +"<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                            + "<tr><td colspan=\"2\">"
                                + "<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead><tr>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">No.</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Case ID</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Issue ID</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Created Date</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Channel</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">IDCard</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Contact Name</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Transaction Date</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Phone Number</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Main Case Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Sub Case Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Commerce Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Commerce Channel</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Product Category</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Service Category</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Delivery Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Payment Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Value Range</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Conversation Channel</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Vendor ID</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Detail</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Reference</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">รายละเอียดการจัดการเคส</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Status</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Close Reason</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">หน่วยงานที่ส่งเรื่องต่อ</th>" 
                        + "</tr></thead><tbody>");
            int irows = 0;
            foreach (var item in rptCase)
            {
                irows++;
                Response.Write("<tr>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + irows + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casIDName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.issID + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casCreatedOn.Value.ToString("yyyy-MM-dd HH:mm:ss") + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.chnName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">&#8203;" + item.ctaNote + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.ctaFullName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.caseventDate + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">&#8203;" + item.ctaNumber + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casLevel1 + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casLevel2 + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.cascommerceType + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casLevel6 + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casproductCategory + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casserviceCategory + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casdeliveryType + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.caspaymentType + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casvalueRange + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casconversationChannel + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casvendorID + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casdetail + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casreferenceDetail + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casSolution + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.cssName + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.casstatusReason + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.ctaEmail + "</td>");
                Response.Write("</tr>");
            }
            Response.Write("</tbody></table></td></tr></table>");
            Response.End();
        }

        #endregion

        #region rptCaseRaw

        public ActionResult rptCaseRaw(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            DataTable dt = new CaseRepository().GetCaseReport(caseIDLevel1, caseIDLevel2, caseIDLevel3, caseIDLevel4, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       {
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           casID = Convert.ToInt32(dr["casID"]),
                           casIDName = dr["casIDName"].ToString(),
                           chnID = dr["chnID"].ToString(),
                           casLevel2 = dr["casLevel2"].ToString(),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           cascommerceType = dr["cascommerceType"].ToString(),
                           casLevel3 = dr["casLevel3"].ToString(),
                           casLevel6 = dr["casLevel6"].ToString(),
                           casreferenceDetail = dr["casreferenceDetail"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           ctaEmail = dr["ctaEmail"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           caseventDate = ConvertEventDate(dr["caseventDate"].ToString()),
                           ctaCareer = dr["ctaCareer"].ToString(),
                           //casFollowDesc = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           casFollowDesc = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           cssName = dr["cssName"].ToString(),
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString(),
                           casAttachFile = dr["casAttachFile"].ToString(),// != null && dr["casAttachFile"].ToString().Split('|').Length > 1 ? dr["casAttachFile"].ToString().Split('|')[1] : "",
                       }).ToList();


            rptListCase.list_repcase = rptCase;
            ViewBag.startDate = startDate;
            ViewBag.endDate = endDate;
            ViewBag.caseIDLevel1 = caseIDLevel1;
            ViewBag.caseIDLevel2 = caseIDLevel2;
            ViewBag.caseIDLevel3 = caseIDLevel3;
            ViewBag.caseIDLevel4 = caseIDLevel4;
            return PartialView(rptListCase);
        }

        public void excelCaseLaw(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CaseDetailReportLaw_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");

            DataTable dt = new CaseRepository().GetCaseReport(caseIDLevel1, caseIDLevel2, caseIDLevel3, caseIDLevel4, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       {
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           casID = Convert.ToInt32(dr["casID"]),
                           casIDName = dr["casIDName"].ToString(),
                           chnID = dr["chnID"].ToString(),
                           casLevel2 = dr["casLevel2"].ToString(),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           cascommerceType = dr["cascommerceType"].ToString(),
                           casLevel3 = dr["casLevel3"].ToString(),
                           casLevel6 = dr["casLevel6"].ToString(),
                           casreferenceDetail = dr["casreferenceDetail"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           ctaEmail = dr["ctaEmail"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           caseventDate = ConvertEventDate(dr["caseventDate"].ToString()),
                           ctaCareer = dr["ctaCareer"].ToString(),
                           //casFollowDesc = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           casFollowDesc = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           cssName = dr["cssName"].ToString(),
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString(),
                           casAttachFile = dr["casAttachFile"].ToString(),// != null && dr["casAttachFile"].ToString().Split('|').Length > 1 ? dr["casAttachFile"].ToString().Split('|')[1] : "",
                       }).ToList();

            Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                            + "<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">Case Detail Report (ร้องเรียนกระทำผิด พรบ/ เว็บไซต์ผิดกฎหมาย)</td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\"></td></tr>"
                            + "<tr><td colspan=\"2\">"
                                + "<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead><tr>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">No.</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Created Date</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Event Date</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Case ID</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Issue ID</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Channel</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Case Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" colspan=\"5\">Contact Info</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" colspan=\"4\">Reference Content</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Detail</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">รายละเอียดการจัดการเคส</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">หน่วยงานที่ส่งเรื่องต่อ</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Status</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Close Reason</th>"
                            + "</tr>"
                            + "<tr>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">IDCard</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Contact Name</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Contact Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Contact email</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Phone Number</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Source Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Reference ID</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Reference Detail</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Attached file</th>" 
                            + "</tr></thead><tbody>");
            int irows = 0;
            foreach (var item in rptCase)
            {
                irows++;
                Response.Write("<tr>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + irows + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casCreatedOn.Value.ToString("yyyy-MM-dd HH:mm:ss") + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.caseventDate + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casIDName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.issID + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.chnName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casLevel2 + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">&#8203;" + item.ctaNote + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.ctaFullName + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.ctaCareer + "</td>");
                Response.Write("<td style=\"text-align:center;vertical-align:top;\">" + item.ctaEmail + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">&#8203;" + item.ctaNumber + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casLevel3 + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casLevel6 + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casreferenceDetail + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casAttachFile + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casdetail + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casSolution + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casFollowDesc + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.cssName + "</td>");
                Response.Write("<td style=\"text-align:left;vertical-align:top;\">" + item.casstatusReason + "</td>");
                Response.Write("</tr>");
            }
            Response.Write("</tbody></table></td></tr></table>");
            Response.End();
        }

        #endregion

        #region rptCaseCyb

        public ActionResult rptCaseCyb(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            DataTable dt = new CaseRepository().GetCaseReport(caseIDLevel1, caseIDLevel2, caseIDLevel3, caseIDLevel4, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       {
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           casID = Convert.ToInt32(dr["casID"]),
                           casIDName = dr["casIDName"].ToString(),
                           chnID = dr["chnID"].ToString(),
                           casLevel2 = dr["casLevel2"].ToString(),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           casLevel3 = dr["casLevel3"].ToString(),
                           casLevel6 = dr["casLevel6"].ToString(),
                           casreferenceDetail = dr["casreferenceDetail"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           ctaEmail = dr["ctaEmail"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           caseventDate = ConvertEventDate(dr["caseventDate"].ToString()),
                           casAttachFile = dr["casAttachFile"].ToString(), //!= null && dr["casAttachFile"].ToString().Split('|').Length > 1 ? dr["casAttachFile"].ToString().Split('|')[1] : "" ,
                           ctaCareer = dr["ctaCareer"].ToString(),
                           //casFollowDesc = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           casFollowDesc = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           cssName = dr["cssName"].ToString(),
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString()
                       }).ToList();


            rptListCase.list_repcase = rptCase;
            ViewBag.startDate = startDate;
            ViewBag.endDate = endDate;
            ViewBag.caseIDLevel1 = caseIDLevel1;
            ViewBag.caseIDLevel2 = caseIDLevel2;
            ViewBag.caseIDLevel3 = caseIDLevel3;
            ViewBag.caseIDLevel4 = caseIDLevel4;
            return PartialView(rptListCase);
        }

        public void excelCaseCyb(DateTime startDate, DateTime endDate, int caseIDLevel1, int caseIDLevel2, int caseIDLevel3, int caseIDLevel4)
        {
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=CaseDetailReportCyber_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");

            DataTable dt = new CaseRepository().GetCaseReport(caseIDLevel1, caseIDLevel2, caseIDLevel3, caseIDLevel4, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
            ListRepCaseModel rptListCase = new ListRepCaseModel();
            List<RepCaseModel> rptCase = new List<RepCaseModel>();
            rptCase = (from DataRow dr in dt.Rows
                       select new RepCaseModel()
                       {
                           casCreatedOn = Convert.ToDateTime(dr["casCreatedOn"].ToString()),
                           casID = Convert.ToInt32(dr["casID"]),
                           casIDName = dr["casIDName"].ToString(),
                           chnID = dr["chnID"].ToString(),
                           casLevel2 = dr["casLevel2"].ToString(),
                           chnName = dr["chnName"].ToString(),
                           ctaFullName = dr["ctaFullName"].ToString(),
                           cascommerceType = dr["cascommerceType"].ToString(),
                           casLevel3 = dr["casLevel3"].ToString(),
                           casLevel6 = dr["casLevel6"].ToString(),
                           casreferenceDetail = dr["casreferenceDetail"].ToString(),
                           casdetail = dr["casdetail"].ToString(),
                           ctaEmail = dr["ctaEmail"].ToString(),
                           ctaNumber = dr["phnNumber"].ToString(),
                           caseventDate = ConvertEventDate(dr["caseventDate"].ToString()),
                           casAttachFile = dr["casAttachFile"].ToString(),// !=null && dr["casAttachFile"].ToString().Split('|').Length > 1 ? dr["casAttachFile"].ToString().Split('|')[1] : "",
                           ctaCareer = dr["ctaCareer"].ToString(),
                           //casFollowDesc = getDepartment(int.Parse(dr["casIDLevel2"].ToString())), //หน่วยงานที่ส่งเรื่องต่อ
                           casFollowDesc = getDepartmentName(int.Parse(dr["casIDLevel2"].ToString()), new CaseRepository().CaseMailToDeptByCaseId(dr["casID"].ToString())),
                           casstatusReason = (dr["cssID"].ToString() == "4") ? dr["casstatusReason"].ToString() : "",
                           cssName = dr["cssName"].ToString(),
                           casSolution = dr["casSolution"].ToString(),
                           ctaNote = dr["ctaNote"].ToString(),
                           issID = dr["issid"].ToString()
                       }).ToList(); 

            Response.Write("<table cellpadding=\"5\" width=\"100%\" align=\"center\">"
                            + "<tr><td align=\"left\" style=\"font-size:20pt;font-weight:bold;vertical-align:middle;height:50px;\">ศูนย์รับเรื่องร้องเรียนปัญหาออนไลน์ 1212 OCC</td><td align=\"right\"><img src='http://52.76.81.218/etdalogo.jpg' /></td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"font-size:16pt;font-weight:bold;vertical-align:middle;height:40px;\">Case Detail Report (ร้องเรียนภัยคุกคาม CYBER)</td></tr>"
                            + "<tr><td colspan=\"2\" align=\"left\" style=\"vertical-align:middle;height:30px;\">Report of " + startDate.ToString("dd MMM yyy") + " to " + endDate.ToString("dd MMM yyy") + " </td></tr>"
                            + "<tr><td colspan=\"2\">"
                                + "<table border=\"1\" width=\"100%\" cellpadding=\"5\"><thead><tr>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">No.</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Created Date</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Event Date</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Case ID</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Issue ID</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Channel</th>"
                                + "<th style =\"background-color:#366092;color:#ffffff;font-weight:bold;\" colspan=\"5\">Contact Info</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Sub Case Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" colspan=\"4\">Reference Content</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Detail</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">รายละเอียดการจัดการเคส</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">หน่วยงานที่ส่งเรื่องต่อ</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Status</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\" rowspan=\"2\">Close Reason</th>"
                            + "</tr>"
                            + "<tr>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">IDCard</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Contact Name</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Contact Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Contact email</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Phone Number</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Source Type</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Reference ID</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Reference Detail</th>"
                                + "<th style=\"background-color:#366092;color:#ffffff;font-weight:bold;\">Attached file</th>" 
                            + "</tr></thead><tbody>");
            int irows = 0;
            foreach (var item in rptCase)
            {
                irows++;
                Response.Write("<tr>");
                Response.Write("<td>" + irows + "</td>");
                Response.Write("<td>" + item.casCreatedOn.Value.ToString("yyyy-MM-dd HH:mm:ss") + "</td>");
                Response.Write("<td>" + item.caseventDate  + "</td>");
                Response.Write("<td>" + item.casIDName + "</td>");
                Response.Write("<td>" + item.issID + "</td>");
                Response.Write("<td>" + item.chnName + "</td>");
                Response.Write("<td>&#8203;" + item.ctaNote + "</td>");
                Response.Write("<td>" + item.ctaFullName + "</td>");
                Response.Write("<td>" + item.ctaCareer + "</td>");
                Response.Write("<td>" + item.ctaEmail + "</td>");
                Response.Write("<td>&#8203;" + item.ctaNumber + "</td>");
                Response.Write("<td>" + item.casLevel2 + "</td>");
                Response.Write("<td>" + item.casLevel3 + "</td>");
                Response.Write("<td>" + item.casLevel6 + "</td>");
                Response.Write("<td>" + item.casreferenceDetail + "</td>");
                Response.Write("<td>" +  item.casAttachFile + "</td>");
                Response.Write("<td>" + item.casdetail + "</td>");
                Response.Write("<td>" + item.casSolution + "</td>");
                Response.Write("<td>" + item.casFollowDesc + "</td>");
                Response.Write("<td>" + item.cssName + "</td>");
                Response.Write("<td>" + item.casstatusReason + "</td>");

                Response.Write("</tr>");
            }
            Response.Write("</tbody></table></td></tr></table>");
            Response.End();
        }

        #endregion
    }
}

