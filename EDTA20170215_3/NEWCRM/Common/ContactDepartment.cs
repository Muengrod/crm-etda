﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NEWCRM.Common
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class contact
    {

        private contactDepartment[] departmentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("department")]
        public contactDepartment[] department
        {
            get
            {
                return this.departmentField;
            }
            set
            {
                this.departmentField = value;
            }
        }
    }

    /// <remarks/>
    public partial class contactDepartment
    {

        private int seqField;

        private string codeField;

        private string nameField;

        private string mailtoField;

        private string mailccField;

        private string casetypeField;

        /// <remarks/>
        public int seq
        {
            get
            {
                return this.seqField;
            }
            set
            {
                this.seqField = value;
            }
        }

        /// <remarks/>
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string mailto
        {
            get
            {
                return this.mailtoField;
            }
            set
            {
                this.mailtoField = value;
            }
        }

        /// <remarks/>
        public string mailcc
        {
            get
            {
                return this.mailccField;
            }
            set
            {
                this.mailccField = value;
            }
        }

        /// <remarks/>
        public string casetype
        {
            get
            {
                return this.casetypeField;
            }
            set
            {
                this.casetypeField = value;
            }
        }
    }


}